package org.exlepse.xle.editor;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

/**
 * <code>Application</code>. This class controls all aspects of the application's execution
 * 
 * <pre>
 * Date: Jul 2, 2010
 * Time: 3:33:13 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: Application.java 8302 2010-07-06 22:43:06Z raedle $
 * @since 1.0.0
 */
public class Application implements IApplication {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app.IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception {
		Display display = PlatformUI.createDisplay();
		try {

			// WorkbenchAdvisor configures the workbench properly.
			ApplicationWorkbenchAdvisor advisor = new ApplicationWorkbenchAdvisor();

			// Start Eclipse workbench
			int returnCode = PlatformUI.createAndRunWorkbench(display, advisor);

			// Restarts the workbench if necessary.
			if (returnCode == PlatformUI.RETURN_RESTART) return IApplication.EXIT_RESTART;
			else return IApplication.EXIT_OK;
		}
		finally {
			display.dispose();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop() {
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null) return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable() {
			public void run() {
				if (!display.isDisposed()) workbench.close();
			}
		});
	}
}
