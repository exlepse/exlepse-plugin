/**
 * 
 */
package org.exlepse.xle.editor.action;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.navigator.CommonViewer;
import org.eclipse.ui.navigator.resources.ProjectExplorer;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

/**
 * <code>SetAsProjectXLE</code>.
 * 
 * <pre>
 * Date: Aug 5, 2010
 * Time: 5:55:17 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class SetAsProjectXLE implements IObjectActionDelegate {

	private IFile file;
	private CommonViewer commonViewer;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction,
	 * org.eclipse.ui.IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		if (targetPart instanceof ProjectExplorer) {
			commonViewer = ((ProjectExplorer) targetPart).getCommonViewer();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) {
		try {
			file.getProject().setPersistentProperty(XLEPreferences.XLE_PROJECT_MAIN,
					file.getProjectRelativePath().toString());
			commonViewer.refresh(true);
		}
		catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
	 * org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			Object element = ((StructuredSelection) selection).getFirstElement();
			if (element instanceof IFile) {
				file = (IFile) element;
				return;
			}
		}
		file = null;
		return;
	}
}
