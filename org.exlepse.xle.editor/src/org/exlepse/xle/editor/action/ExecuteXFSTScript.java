/**
 * 
 */
package org.exlepse.xle.editor.action;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.common.XFSTProcess;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

/**
 * <code>CompileXFST</code>.
 * 
 * <pre>
 * Date: Aug 12, 2010
 * Time: 11:50:52 AM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: ExecuteXFSTScript.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.2
 */
public class ExecuteXFSTScript implements IObjectActionDelegate {

	private IFile file;
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// empty
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) {
		try {
			XFSTProcess process = new XFSTProcess(file, "XFST", "UTF-8");
			file.getParent().refreshLocal(IResource.DEPTH_ONE, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof StructuredSelection) {
			Object element = ((StructuredSelection) selection).getFirstElement();
			if (element instanceof IFile) {
				file = (IFile) element;
				return;
			}
		}
		file = null;
		return;
	}
}
