/**
 * 
 */
package org.exlepse.xle.editor.action;

import java.io.File;
import java.net.MalformedURLException;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

/**
 * <code>OpenDocumenationAction</code>.
 * 
 * <pre>
 * Date: Jul 7, 2010
 * Time: 12:31:44 AM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class OpenDocumenationAction implements IWorkbenchWindowActionDelegate {

	public void dispose() {
		// TODO Auto-generated method stub

	}

	public void init(IWorkbenchWindow window) {
		// TODO Auto-generated method stub

	}

	public void run(IAction action) {
		try {

			IPreferenceStore preferenceStore = XLEPlugin.getDefault().getPreferenceStore();
			String documentationDirectory = preferenceStore.getString(XLEPreferences.DOCUMENTATION_PATH);

			File file = new File(documentationDirectory, "xle_toc.html");
			if (!file.exists()) {
				// message.
			}

			try {
				PlatformUI.getWorkbench().getBrowserSupport().createBrowser(IWorkbenchBrowserSupport.AS_EDITOR,
						"Documentation", "XLE Documentation", "XLE Documentation").openURL(file.toURL());
			}
			catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch (PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub

	}
}
