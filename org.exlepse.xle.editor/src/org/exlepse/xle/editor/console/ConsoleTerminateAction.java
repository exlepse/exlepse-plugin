/**
 * 
 */
package org.exlepse.xle.editor.console;

import org.eclipse.jface.action.Action;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.texteditor.IUpdate;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.common.AbstractProcess;

/**
 * <code>ConsoleTerminateAction</code>.
 * 
 * <pre>
 * Date: Aug 6, 2010
 * Time: 8:10:12 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class ConsoleTerminateAction extends Action implements IUpdate {

	private final IWorkbenchWindow fWindow;
	private ProcessConsole fConsole;

	public ConsoleTerminateAction(IWorkbenchWindow window, ProcessConsole console) {
		super("&Terminate");
		fWindow = window;
		fConsole = console;
		setImageDescriptor(XLEPlugin.getImageDescriptor("icons/full/obj16/terminate_co.gif"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.IUpdate#update()
	 */
	public void update() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.action.Action#run()
	 */
	@Override
	public void run() {
		super.run();

		AbstractProcess process = fConsole.getAbstractProcess();
		process.destroy();
	}

	/**
	 * 
	 */
	public void dispose() {
		fConsole = null;
	}
}
