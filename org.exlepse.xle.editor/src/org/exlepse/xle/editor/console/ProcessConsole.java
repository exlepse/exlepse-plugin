/**
 * 
 */
package org.exlepse.xle.editor.console;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.console.IOConsole;
import org.exlepse.xle.editor.common.AbstractProcess;

/**
 * <code>ProcessConsole</code>.
 * 
 * <pre>
 * Date: Aug 6, 2010
 * Time: 8:06:30 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class ProcessConsole extends IOConsole {

	private final AbstractProcess fProcess;

	public ProcessConsole(AbstractProcess process, String name, String consoleType, ImageDescriptor imageDescriptor,
			String encoding, boolean autoLifecycle) {
		super(name, consoleType, imageDescriptor, encoding, autoLifecycle);
		fProcess = process;
	}

	/**
	 * @return
	 */
	public AbstractProcess getAbstractProcess() {
		return fProcess;
	}
}
