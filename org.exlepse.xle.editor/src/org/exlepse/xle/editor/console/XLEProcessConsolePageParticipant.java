/**
 * 
 */
package org.exlepse.xle.editor.console;

import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsolePageParticipant;
import org.eclipse.ui.part.IPageBookViewPage;

/**
 * <code>TerminateConsoleActionDelegate</code>.
 * 
 * <pre>
 * Date: Aug 6, 2010
 * Time: 7:40:13 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class XLEProcessConsolePageParticipant implements IConsolePageParticipant {

	private IPageBookViewPage fPage;
	private ProcessConsole fConsole;

	private ConsoleTerminateAction fTerminate;

	public void init(IPageBookViewPage page, IConsole console) {
		fPage = page;
		fConsole = (ProcessConsole) console;

		fTerminate = new ConsoleTerminateAction(page.getSite().getWorkbenchWindow(), fConsole);

		// contribute to toolbar
		IActionBars actionBars = fPage.getSite().getActionBars();
		configureToolBar(actionBars.getToolBarManager());
	}

	public void activated() {
		// TODO Auto-generated method stub

	}

	public void deactivated() {
		// TODO Auto-generated method stub

	}

	public void dispose() {
		if (fTerminate != null) {
			fTerminate.dispose();
			fTerminate = null;
		}
	}

	/**
	 * Contribute actions to the toolbar
	 */
	protected void configureToolBar(IToolBarManager mgr) {
		mgr.appendToGroup(IConsoleConstants.LAUNCH_GROUP, fTerminate);
	}

	public Object getAdapter(Class adapter) {
		// if (IShowInSource.class.equals(adapter)) {
		// return this;
		// }
		// if (IShowInTargetList.class.equals(adapter)) {
		// return this;
		// }
		// //CONTEXTLAUNCHING
		// if(ILaunchConfiguration.class.equals(adapter)) {
		// ILaunch launch = getProcess().getLaunch();
		// if(launch != null) {
		// return launch.getLaunchConfiguration();
		// }
		// return null;
		// }
		return null;
	}
}
