package org.exlepse.xle.editor;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.exlepse.xle.editor.common.XLEProcess;
import org.osgi.framework.BundleContext;

/**
 * <code>XLEPlugin</code>. The activator class controls the plug-in life cycle
 * 
 * <pre>
 * Date: Jul 2, 2010
 * Time: 2:44:04 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: XLEPlugin.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class XLEPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String ID = "org.exlepse.xle.editor";

	// The shared instance
	private static XLEPlugin plugin;

	/**
	 * The constructor
	 */
	public XLEPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(final BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);

		for (Process process : XLEProcess.XLE_PROCESSES) {
			process.destroy();
		}
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static XLEPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given plug-in relative path
	 * 
	 * @param path
	 *            the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(ID, path);
	}

}
