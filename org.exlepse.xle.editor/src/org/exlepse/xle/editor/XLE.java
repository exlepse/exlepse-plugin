package org.exlepse.xle.editor;

import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

/**
 * <code>XLE</code>.
 * 
 * <pre>
 * Date: Jul 4, 2010
 * Time: 12:26:41 AM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: XLE.java 9142 2010-08-07 21:58:29Z raedle $
 * @since 1.0.0
 */
public class XLE implements IPerspectiveFactory {

	public static final String ID = "org.exlepse.xle.editor.XLEPerspective";

	public void createInitialLayout(IPageLayout layout) {
		String editorArea = layout.getEditorArea();
		layout.setEditorAreaVisible(true);
		// layout.setFixed(true);
		// layout.addStandaloneView(SectionView.ID, false, IPageLayout.LEFT, 0.3f, editorArea);
		// layout.addView(SectionView.ID, IPageLayout.LEFT, 0.3f, editorArea);
		layout.addView("org.eclipse.ui.navigator.ProjectExplorer", IPageLayout.LEFT, 0.2f, editorArea);
	}
}
