/**
 * 
 */
package org.exlepse.xle.editor.decorators;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.IDecoration;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ILightweightLabelDecorator;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

/**
 * <code>MainGrammarDecorator</code>.
 * 
 * <pre>
 * Date: Aug 5, 2010
 * Time: 12:46:32 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class MainGrammarDecorator implements ILightweightLabelDecorator {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.ILightweightLabelDecorator#decorate(java.lang.Object,
	 * org.eclipse.jface.viewers.IDecoration)
	 */
	public void decorate(Object element, IDecoration decoration) {
		if (element instanceof IResource) {
			IResource objectResource = (IResource) element;

			if (objectResource.getType() != IResource.FILE) {
				return;
			}

			try {
				String value = objectResource.getProject().getPersistentProperty(XLEPreferences.XLE_PROJECT_MAIN);
				if (value != null) {
					if (objectResource.getProjectRelativePath().toString().equals(value)) {
						decoration.addOverlay(XLEPlugin
								.getImageDescriptor("icons/full/obj10/bullet_triangle_green.png"));
					}
				}
			}
			catch (CoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
	 */
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	public void addListener(ILabelProviderListener listener) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
	 */
	public void removeListener(ILabelProviderListener listener) {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
	 */
	public void dispose() {

	}
}
