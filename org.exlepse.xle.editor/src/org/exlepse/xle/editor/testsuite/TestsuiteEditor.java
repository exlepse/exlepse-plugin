/**
 * 
 */
package org.exlepse.xle.editor.testsuite;

import org.eclipse.ui.editors.text.TextEditor;

/**
 * <code>TestsuiteEditor</code>.
 * 
 * <pre>
 * Date: Aug 5, 2010
 * Time: 11:50:32 AM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class TestsuiteEditor extends TextEditor {

}
