/**
 * 
 */
package org.exlepse.xle.editor.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.IPreferenceStore;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.commands.parse.IParseMarker;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

/**
 * <code>XLEProcess</code>.
 * 
 * <pre>
 * Date: Aug 5, 2010
 * Time: 12:09:46 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class XLEProcess extends AbstractProcess {

	public static final Set<Process> XLE_PROCESSES = new HashSet<Process>();

	private final IProject project;

	// Mapping IProject to XLEProcess
	private static final Map<IProject, XLEProcess> PROJECT_XLE_PROCESSES = new HashMap<IProject, XLEProcess>();

	/**
	 * @param project
	 * @return
	 */
	public static final XLEProcess get(IProject project) throws CoreException {
		if (PROJECT_XLE_PROCESSES.containsKey(project)) {
			return PROJECT_XLE_PROCESSES.get(project);
		}

		XLEProcess process = new XLEProcess(project);
		try {
			process.createParser();
		} catch (CoreException e) {
			process.destroy();
			throw e;
		}
		PROJECT_XLE_PROCESSES.put(project, process);
		return process;
	}

	/**
	 * @param project
	 */
	public static final void update(IProject project) throws CoreException {
		XLEProcess process = get(project);
		try {
			process.createParser();
		} catch (CoreException e) {
			e.printStackTrace();
			process.destroy();
		}
	}

	/**
	 * @param project
	 */
	private XLEProcess(IProject project) throws CoreException {
		super(project.getName(), getCharacterEncoding());
		this.project = project;
	}

	/**
	 * 
	 */
	protected Process createProcess(Object... params) throws CoreException {

		// // This is just a hack to have access to the tk $DISPLAY :0 required
		// to execute
		// // the XLE environment (XLE output).
		// try {
		// Runtime.getRuntime().exec("open " + getX11Path());
		// } catch (IOException e1) {
		// e1.printStackTrace();
		// }

		String xlePath = getXLEPath();
		String xleBinPath = xlePath + "/bin";

		ProcessBuilder pb = new ProcessBuilder(xleBinPath + "/xle");

		Map<String, String> env = pb.environment();
		augmentEnvironment(env, "XLEPATH", xlePath);
		augmentEnvironment(env, "PATH", xleBinPath);
		augmentEnvironment(env, "LD_LIBRARY_PATH", getLDLIBRARYPATH());
		augmentEnvironment(env, "DYLD_LIBRARY_PATH", getDYLDLIBRARYPATH());

		// Sets $DISPLAY variable if necessary
		if (isUseDisplay()) {
			env.put("DISPLAY", getDisplay());
		}

		pb.directory(new File(xleBinPath));

		Process process = null;
		try {
			process = pb.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		XLE_PROCESSES.add(process);

		final InputStream errorStream = process.getErrorStream();
		final InputStream inputStream = process.getInputStream();
		final OutputStream outputStream = process.getOutputStream();

		openErrorStreamThread(errorStream);
		openInputStreamThread(inputStream);
		openOutputStreamThread(outputStream);
		
		return process;
	}

	/**
	 * @throws CoreException .
	 * 
	 */
	public void createParser() throws CoreException {
		String value = null;
		try {
			value = project
					.getPersistentProperty(XLEPreferences.XLE_PROJECT_MAIN);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (value == null) {
			throw new CoreException(
					new Status(
							IStatus.ERROR,
							XLEPlugin.ID,
							"Right-click on a lfg file in the project and select \"XLE > Set As Project XLE\""));
		}

		deleteAllProblemMarkers();

		IFile iFile = project.getFile(value);

		File file = new File(iFile.getLocationURI());

		StringBuilder sb = new StringBuilder();

		// Append "set character encoding {0}"
		sb.append(getCommand(XLEPreferences.CMD_SET_CHARACTER_ENCODING,
				getCharacterEncoding()));

		// Append "create-parser {0}" command
		sb.append(getCommand(XLEPreferences.CMD_CREATE_PARSER, file
				.getAbsolutePath()));

		try {
			fProcess.getOutputStream().write(
					sb.toString().getBytes(getCharacterEncoding()));
			fProcess.getOutputStream().flush();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param text
	 */
	public void parse(String text) {
		try {
			deleteAllProblemMarkers();
			writeTextToOutputStream(fProcess.getOutputStream(), text);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void deleteAllProblemMarkers() {
		// delete previous parser marks
		try {
			project.deleteMarkers(IParseMarker.PROBLEM, true,
					IResource.DEPTH_INFINITE);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param outputStream
	 * @param lfgFile
	 * @param text
	 * @throws IOException
	 */
	private void writeTextToOutputStream(OutputStream outputStream, String text)
			throws IOException {
		String parseCmd = getCommand(XLEPreferences.CMD_PARSE, text);

		outputStream.write(parseCmd.getBytes(getCharacterEncoding()));
		outputStream.flush();
	}

	/**
	 * @param inputStream
	 */
	protected void openInputStreamThread(final InputStream inputStream) {
		new Thread() {

			@Override
			public void run() {
				BufferedReader br = new BufferedReader(new InputStreamReader(
						inputStream));

				String line = null;
				try {
					while ((line = br.readLine()) != null) {
						line = line.trim();
						checkForProblems(line);
						System.out.println(line);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * @param outputStream
	 */
	protected void openOutputStreamThread(final OutputStream outputStream) {
		new Thread() {

			@Override
			public void run() {
				final String characterSet = getCharacterEncoding();

				BufferedReader br = new BufferedReader(new InputStreamReader(
						System.in, Charset.forName(characterSet)));
				try {
					String line = null;
					while ((line = br.readLine()) != null) {
						outputStream.write(line.getBytes(characterSet));
						outputStream.write('\n');
						outputStream.flush();
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * @param environment
	 * @param variable
	 * @param xlePath
	 */
	private void augmentEnvironment(Map<String, String> environment, String variable, String xlePath) {
		String value = environment.get(variable);

		xlePath += value != null ? ":" + value : "";

		environment.put(variable, xlePath);
	}

	/**
	 * @return
	 */
	private static String getX11Path() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getString(XLEPreferences.X11_PATH);
	}

	/**
	 * @return
	 */
	private static String getXLEPath() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getString(XLEPreferences.XLE_PATH);
	}

	/**
	 * @return
	 */
	private static String getLDLIBRARYPATH() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getString(XLEPreferences.LD_LIBRARY_PATH);
	}

	/**
	 * @return
	 */
	private static String getDYLDLIBRARYPATH() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getString(XLEPreferences.DYLD_LIBRARY_PATH);
	}

	/**
	 * @return
	 */
	private static String getCharacterEncoding() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getString(XLEPreferences.LFG_FILE_ENCODING);
	}

	/**
	 * @return
	 */
	private static boolean isUseDisplay() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getBoolean(XLEPreferences.USE_DISPLAY);
	}

	/**
	 * @return
	 */
	private static String getDisplay() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getString(XLEPreferences.DISPLAY);
	}

	/**
	 * @return
	 */
	private static String getCommand(String cmdKey, Object... arguments) {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return MessageFormat.format(preferenceStore.getString(cmdKey) + "\n",
				arguments);
	}

	/**
	 * @return
	 */
	private static String[] getProblemRegexes() {
		IPreferenceStore preferenceStore = getPreferenceStore();
		return preferenceStore.getString(XLEPreferences.PROBLEMS_REGEXES)
				.split("\\;");
	}

	/**
	 * @return
	 */
	private static IPreferenceStore getPreferenceStore() {
		return XLEPlugin.getDefault().getPreferenceStore();
	}

	/**
	 * 
	 */
	public void destroy() {
		XLE_PROCESSES.remove(fProcess);
		PROJECT_XLE_PROCESSES.remove(project);
		
		super.destroy();
	}

	// private static final String[] REGEXES = new String[] {
	// "\\s*Bad meta designator near line ([\\d]+), column ([\\d]+)\\s*",
	// "\\s*parse error at (.*), maybe near line ([\\d]+), column ([\\d]+) in (.*)\\s*",
	// "\\s*Template invocation error near line ([\\d]+), column ([\\d]+)\\s*",
	// "\\s*Possible missing \\e. or quote near headword (.*) line ([\\d]+) file (.*)\\s*",
	// "\\s*parse error at (.*)\\s*" };

	/**
	 * @param line
	 */
	private void checkForProblems(String line) {
		boolean matches = false;

		String[] problemsRegexes = getProblemRegexes();
		for (int i = 0; i < problemsRegexes.length && !matches; i++) {
			if ((matches = line.matches(problemsRegexes[i]))) {
				// IMarker marker = null;
				// switch (i) {
				// case 1:
				// Pattern pattern = Pattern.compile(REGEXES[i]);
				// Matcher matcher = pattern.matcher(line);
				// if (matcher.matches()) {
				// for (int j = 0; j < matcher.groupCount(); j++) {
				// System.out.println(matcher.group(j));
				// }
				// }
				// break;
				// case 3:
				// default:
				// try {
				// marker = project.createMarker(IParseMarker.PARSER_ERROR);
				// }
				// catch (CoreException e) {
				// e.printStackTrace();
				// }
				// break;
				// }
				//
				// try {
				// marker.setAttribute(IMarker.MESSAGE, line);
				// marker.setAttribute(IMarker.USER_EDITABLE, false);
				// marker.setAttribute(IMarker.SEVERITY,
				// IMarker.SEVERITY_ERROR);
				// }
				// catch (CoreException e) {
				// e.printStackTrace();
				// }
				try {
					IMarker marker = project
							.createMarker(IParseMarker.PARSER_ERROR);
					marker.setAttribute(IMarker.MESSAGE, line);
					marker.setAttribute(IMarker.USER_EDITABLE, false);
					marker.setAttribute(IMarker.SEVERITY,
							IMarker.SEVERITY_ERROR);
				} catch (CoreException e) {
					e.printStackTrace();
				}

			}
		}
	}
}
