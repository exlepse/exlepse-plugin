package org.exlepse.xle.editor.common;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.StringConverter;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.exlepse.xle.editor.XLEPlugin;

/**
 * <code>ColorManager</code>.
 *
 * <pre>
 * Date: Jul 2, 2010
 * Time: 3:38:25 PM
 * </pre>
 *
 * @author Roman Raedle
 * @author Michael Zoellner
 *
 * @version $Id: ColorManager.java 9114 2010-08-05 16:34:29Z zoellner $
 * @since 1.0.0
 */
public class ColorManager {
	protected Map fColorTable = new HashMap(10);

	public void dispose() {
		Iterator e = fColorTable.values().iterator();
		while (e.hasNext())
			((Color) e.next()).dispose();
	}

	public Color getColor(RGB rgb) {
		Color color = (Color) fColorTable.get(rgb);
		if (color == null) {
			color = new Color(Display.getCurrent(), rgb);
			fColorTable.put(rgb, color);
		}
		return color;
	}
	
	public Color getColor(String preferenceString)
	{
		return getColor(StringConverter.asRGB(getPreferenceStore().getString(preferenceString)));
	}
	
	private IPreferenceStore getPreferenceStore() {
		return XLEPlugin.getDefault().getPreferenceStore();
	}
}
