/**
 * 
 */
package org.exlepse.xle.editor.common;

import java.io.File;
import java.io.IOException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;


/**
 * <code>XFSTProcess</code>.
 * 
 * <pre>
 * Date: Aug 12, 2010
 * Time: 11:56:35 AM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: XFSTProcess.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.2
 */
public class XFSTProcess extends AbstractProcess {

	/**
	 * @param file
	 * @param name
	 * @param encoding
	 */
	public XFSTProcess(IFile file, String name, String encoding) throws CoreException {
		super(name, encoding, file);
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.editor.common.AbstractProcess#createProcess(java.lang.Object[])
	 */
	@Override
	protected Process createProcess(Object... params) throws CoreException {
		IPreferenceStore preferenceStore = XLEPlugin.getDefault().getPreferenceStore();
		String xfstPath = preferenceStore.getString(XLEPreferences.XFST_PATH);
		File xfstFile = new File(xfstPath);
		
		ProcessBuilder pb = new ProcessBuilder(xfstFile.getAbsolutePath());
		
		File compilingUnit = new File(((IFile) params[0]).getRawLocationURI());
		pb.directory(compilingUnit.getParentFile());
		
		Process process = null;
		try {
			process = pb.start();
			openErrorStreamThread(process.getErrorStream());
			openInputStreamThread(process.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		// Execute script if process available
		if (process != null) {
			StringBuilder sb = new StringBuilder();
			sb.append("source ").append(compilingUnit.getAbsolutePath()).append("\n");
			try {
				process.getOutputStream().write(sb.toString().getBytes());
				process.getOutputStream().flush();
				process.getOutputStream().write("exit\n".getBytes());
				process.getOutputStream().flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				process.waitFor();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return process;
	}
}
