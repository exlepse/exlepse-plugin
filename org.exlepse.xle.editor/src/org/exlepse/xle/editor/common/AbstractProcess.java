/**
 * 
 */
package org.exlepse.xle.editor.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IOConsoleOutputStream;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.console.ProcessConsole;

/**
 * @author raedle
 *
 */
public abstract class AbstractProcess {

	protected ProcessConsole fConsole;
	protected Process fProcess;
	
	/**
	 * @param name
	 */
	public AbstractProcess(String name, String encoding, Object... params) throws CoreException {
		fConsole = createConsole(name, encoding);
		fProcess = createProcess(params);
	}
	
	/**
	 * @return
	 */
	protected abstract Process createProcess(Object... params) throws CoreException;
	
	/**
	 * @param name
	 */
	protected final ProcessConsole createConsole(String name, String encoding) {
		final IConsoleManager consoleManager = ConsolePlugin.getDefault().getConsoleManager();

		ProcessConsole console = new ProcessConsole(this, name, "XLE", XLEPlugin.getImageDescriptor("icons/text_code_colored.png"), encoding, false);

		consoleManager.addConsoles(new IConsole[] { console });
		final IOConsoleOutputStream stream = console.newOutputStream();
		final PrintStream myS = new PrintStream(stream);
		System.setOut(myS);
		System.setErr(myS);
		System.setIn(console.getInputStream());
		
		return console;
	}
	
	/**
	 * @param errorStream
	 */
	protected void openErrorStreamThread(final InputStream errorStream) {
		Thread errorStreamThread = new Thread() {

			@Override
			public void run() {
				Reader reader = new InputStreamReader(errorStream);
				int c;
				try {
					while ((c = reader.read()) != -1) {
						System.err.print((char) c);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		errorStreamThread.start();
	}
	
	/**
	 * @param inputStream
	 */
	protected void openInputStreamThread(final InputStream inputStream) {
		new Thread() {

			@Override
			public void run() {
				Reader reader = new InputStreamReader(inputStream);
				int c;
				try {
					while ((c = reader.read()) != -1) {
						System.out.print((char) c);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}

	/**
	 * Destroys the process.
	 */
	public void destroy() {
		final IConsoleManager consoleManager = ConsolePlugin.getDefault().getConsoleManager();
		consoleManager.removeConsoles(new IConsole[] { fConsole });
		fConsole.destroy();

		try {
			fProcess.getOutputStream().write("exit\n".getBytes());
			fProcess.getOutputStream().flush();
			fProcess.getInputStream().close();
			fProcess.getErrorStream().close();
			fProcess.getOutputStream().close();
			fProcess.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fProcess.destroy();
	}
}
