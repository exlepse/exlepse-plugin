package org.exlepse.xle.editor.common;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.exlepse.xle.editor.XLEPlugin;

public class TokenManager
{
	
	public IToken getToken(String preferenceString)
	{
		//Token preference string format = "font color, bold (boolean), italic (boolean)"
		String tokenPreference = getPreferenceStore().getString(preferenceString);
		
		String[] tokenValues = tokenPreference.split(",");
		
		return Token.UNDEFINED;
	}

	private IPreferenceStore getPreferenceStore() {
		return XLEPlugin.getDefault().getPreferenceStore();
	}
	
}
