/**
 * 
 */
package org.exlepse.xle.editor.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.exlepse.xle.editor.XLEPlugin;

/**
 * <code>XLEUtils</code>.
 * 
 * <pre>
 * Date: Jul 12, 2010
 * Time: 8:34:45 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class XLEUtils {

	public static void createParser(IFile file) throws CoreException {
		final StringBuilder sb = new StringBuilder();

		try {
			final File lfgFile = new File(file.getLocationURI());

			ProcessBuilder pb = new ProcessBuilder("/usr/local/XLE/bin/xle");
			pb.directory(new File("/usr/local/XLE"));

			final Process process = pb.start();

			// final Process process =
			// Runtime.getRuntime().exec("/usr/local/xle/bin/xle");
			// final Process process = Runtime.getRuntime().exec("bash", null, new
			// File("/usr/bin"));

			new Thread() {

				@Override
				public void run() {
					BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
					char[] buf = new char[8096];
					int len;
					try {
						while ((len = br.read(buf)) != -1) {
							// System.err.println(String.valueOf(buf, 0, len));
							sb.append(buf, 0, len);
						}
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}.start();

			new Thread() {

				@Override
				public void run() {
					BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
					char[] buf = new char[8096];
					int len;
					try {
						while ((len = br.read(buf)) != -1) {
							// System.out.println(String.valueOf(buf, 0, len));
							sb.append(buf, 0, len);
						}
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}.start();

			process.getOutputStream().write("create-parser".getBytes());
			process.getOutputStream().write(" ".getBytes());
			process.getOutputStream().write(lfgFile.getAbsolutePath().getBytes());
			process.getOutputStream().write("\n".getBytes());
			process.getOutputStream().flush();

			// TODO: This is just a hack to wait until X11 and XLE has been started (create-parser).
			try {
				Thread.sleep(500);
			}
			catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (sb.indexOf("Lex Warning") != -1) {
			int beginWarning = sb.indexOf("Lex Warning");
			int endWarning = sb.indexOf("\n", beginWarning);

			String errorMessage = sb.substring(beginWarning, endWarning);

			IStatus status = new Status(IStatus.ERROR, XLEPlugin.getDefault().getDescriptor().getUniqueIdentifier(), 0,
					errorMessage, null);

			throw new CoreException(status);
		}
		else if (sb.indexOf("parse error") != -1) {
			int beginWarning = sb.indexOf("parse error");
			int endWarning = sb.indexOf("\n", beginWarning);

			String errorMessage = sb.substring(beginWarning, endWarning);

			IStatus status = new Status(IStatus.ERROR, XLEPlugin.getDefault().getDescriptor().getUniqueIdentifier(), 0,
					errorMessage, null);

			throw new CoreException(status);
		}
	}
}
