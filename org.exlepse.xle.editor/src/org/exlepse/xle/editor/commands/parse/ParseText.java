package org.exlepse.xle.editor.commands.parse;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.part.FileEditorInput;
import org.exlepse.xle.editor.common.XLEProcess;

/**
 * <code>ParseText</code>.
 * 
 * <pre>
 * Date: Aug 5, 2010
 * Time: 12:04:06 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class ParseText extends AbstractParseText implements IEditorActionDelegate, IHandler {

	private IEditorPart targetEditor;
	private IProject project;
	private String text;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IEditorActionDelegate#setActiveEditor(org.eclipse.jface.action.IAction,
	 * org.eclipse.ui.IEditorPart)
	 */
	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		this.targetEditor = targetEditor;

		if (targetEditor == null) {
			return;
		}

		if (targetEditor instanceof TextEditor) {
			IEditorInput editorInput = ((TextEditor) targetEditor).getEditorInput();

			if (editorInput instanceof FileEditorInput) {
				IFile file = ((FileEditorInput) editorInput).getFile();
				project = file.getProject();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) {
		if (project != null) {

			try {
				XLEProcess process = XLEProcess.get(project);
				process.parse(text);
				IMarker marker = getMissingProjectXLEMarker(project);
				if (marker != null) {
					marker.delete();
				}
			}
			catch (CoreException e) {
				try {
					IMarker marker = getMissingProjectXLEMarker(project);
					if (marker == null) {
						marker = project.createMarker(IParseMarker.MISSING_PROJECT_XLE);
					}
					marker.setAttribute(IMarker.MESSAGE, e.getMessage());
					marker.setAttribute(IMarker.USER_EDITABLE, false);
					// marker.setAttribute(IMarker.TRANSIENT, true);
					marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
				}
				catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @param project
	 * @return
	 */
	private IMarker getMissingProjectXLEMarker(IProject project) {
		try {
			IMarker[] markers = project.findMarkers(IParseMarker.MISSING_PROJECT_XLE, false, IResource.DEPTH_ZERO);
			return markers.length > 0 ? markers[0] : null;
		}
		catch (CoreException e) {
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction,
	 * org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		if (selection instanceof TextSelection) {
			TextSelection textSelection = (TextSelection) selection;
			text = textSelection.getText();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {

		IEditorPart editorPart = HandlerUtil.getActiveEditor(event);
		if (editorPart instanceof TextEditor) {
			TextEditor textEditor = (TextEditor) editorPart;

			ISelectionProvider selectionProvider = ((TextEditor) editorPart).getSelectionProvider();
			ISelection selection = selectionProvider.getSelection();

			if (selection instanceof TextSelection) {
				String text = ((TextSelection) selection).getText();

				IEditorInput editorInput = textEditor.getEditorInput();

				if (editorInput instanceof FileEditorInput) {
					IFile file = ((FileEditorInput) editorInput).getFile();

					parse(file.getProject(), text);
				}
			}
		}

		return null;
	}
}
