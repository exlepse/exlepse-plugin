/**
 * 
 */
package org.exlepse.xle.editor.commands.parse;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.exlepse.xle.editor.common.XLEProcess;

/**
 * <code>ParseText</code>.
 * 
 * <pre>
 * Date: Aug 6, 2010
 * Time: 4:59:35 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public abstract class AbstractParseText extends AbstractHandler implements IHandler {

	/**
	 * @param project
	 * @param text
	 * @return
	 */
	public Object parse(IProject project, String text) {
		try {
			XLEProcess process = XLEProcess.get(project);
			process.parse(text);
			IMarker marker = getMissingProjectXLEMarker(project);
			if (marker != null) {
				marker.delete();
			}
		}
		catch (CoreException e) {
			try {
				IMarker marker = getMissingProjectXLEMarker(project);
				if (marker == null) {
					marker = project.createMarker(IParseMarker.MISSING_PROJECT_XLE);
				}
				marker.setAttribute(IMarker.MESSAGE, e.getMessage());
				marker.setAttribute(IMarker.TRANSIENT, true);
				marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
			}
			catch (CoreException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * @param project
	 * @return
	 */
	private IMarker getMissingProjectXLEMarker(IProject project) {
		try {
			IMarker[] markers = project.findMarkers(IParseMarker.MISSING_PROJECT_XLE, false, IResource.DEPTH_ZERO);
			if (markers != null && markers.length > 0) {
				return markers[0];
			}
		}
		catch (CoreException e) {
			e.printStackTrace();
		}
		return null;
	}
}
