package org.exlepse.xle.editor.commands.parse;

import org.eclipse.core.resources.IMarker;

/**
 * <code>IParseMarker</code>.
 * 
 * <pre>
 * Date: Aug 6, 2010
 * Time: 8:51:10 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public interface IParseMarker extends IMarker {
	public static final String PROBLEM = "org.exlepse.xle.editor.problemmarker";

	public static final String MISSING_PROJECT_XLE = "org.exlepse.xle.editor.missingprojectxle";
	public static final String PARSER_ERROR = "org.exlepse.xle.editor.parsererrormarker";
}
