/**
 * 
 */
package org.exlepse.xle.editor.commands.parse;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.navigator.resources.ProjectExplorer;

/**
 * <code>parseTextInput</code>.
 * 
 * <pre>
 * Date: Aug 6, 2010
 * Time: 4:59:51 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class ParseTextInput extends AbstractParseText implements IHandler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow workbenchWindow = HandlerUtil.getActiveWorkbenchWindow(event);
		ISelectionService selectionService = workbenchWindow.getSelectionService();
		IStructuredSelection structuredSelection = (IStructuredSelection) selectionService
				.getSelection(ProjectExplorer.VIEW_ID);

		if (structuredSelection == null) {
			MessageDialog.openInformation(null, "Project", "Please select a project");
			return null;
		}
		
		IResource resource = (IResource) structuredSelection.getFirstElement();
		if (resource == null) {
			MessageDialog.openInformation(null, "Project", "Please select a project");
			return null;
		}

		InputDialog dialog = new InputDialog(workbenchWindow.getShell(), "Parse", "Parse", "", null);
		int returnCode = dialog.open();
		if (returnCode != InputDialog.OK) return null;

		String text = dialog.getValue();

		parse(resource.getProject(), text);

		return null;
	}
}
