package org.exlepse.xle.editor;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.application.ActionBarAdvisor;
import org.eclipse.ui.application.IActionBarConfigurer;

/**
 * <code>ApplicationActionBarAdvisor</code>.
 *
 * <pre>
 * Date: Jul 2, 2010
 * Time: 3:33:05 PM
 * </pre>
 *
 * @author Michael Zoellner
 * @author Roman Raedle
 *
 * @version $Id: ApplicationActionBarAdvisor.java 9041 2010-08-02 13:59:44Z zoellner $
 * @since 1.0.0
 */
public class ApplicationActionBarAdvisor extends ActionBarAdvisor {

    public ApplicationActionBarAdvisor(IActionBarConfigurer configurer) {
        super(configurer);
    }

    protected void makeActions(IWorkbenchWindow window) {
//    	ActionFactory.OPEN_PERSPECTIVE_DIALOG.create(window);
//    	ActionFactory.RESET_PERSPECTIVE.create(window);
//    	ActionFactory.ABOUT.create(window);
//    	ActionFactory.HELP_CONTENTS.create(window);
//    	ActionFactory.HELP_SEARCH.create(window);
    }

    protected void fillMenuBar(IMenuManager menuBar) {
//    	menuBar.add(new GroupMarker(IWorkbenchActionConstants.BUILD));
//    	menuBar.add(new GroupMarker(IWorkbenchActionConstants.MB_ADDITIONS));
//    	menuBar.add(new GroupMarker(IWorkbenchActionConstants.MOVE));
    }
}
