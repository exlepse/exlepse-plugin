/**
 * 
 */
package org.exlepse.xle.editor.lfg;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchActionConstants;
import org.eclipse.ui.editors.text.TextEditorActionContributor;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.ITextEditorActionDefinitionIds;
import org.eclipse.ui.texteditor.RetargetTextEditorAction;

/**
 * <code>RulesSectionEditorActionContributor</code>.
 *
 * <pre>
 * Date: Jun 30, 2010
 * Time: 8:51:46 PM
 * </pre>
 *
 * @author Roman Raedle
 * @author Michael Zoellner
 *
 * @version $Id: RulesSectionEditorActionContributor.java 8100 2010-07-02 18:18:19Z raedle $
 * @since 1.0.0
 */
public class LfgEditorActionContributor extends TextEditorActionContributor {

	protected RetargetTextEditorAction fContentAssistProposal;
	protected RetargetTextEditorAction fContentFormatProposal;
	
	
	public LfgEditorActionContributor()
	{
		fContentAssistProposal = new RetargetTextEditorAction(LfgEditorMessages.getResourceBundle(),"ContentAssistProposal.");
		fContentAssistProposal.setActionDefinitionId(ITextEditorActionDefinitionIds.CONTENT_ASSIST_PROPOSALS);
		fContentAssistProposal.setAccelerator(SWT.CTRL | ' ');

		
		fContentFormatProposal = new RetargetTextEditorAction(LfgEditorMessages.getResourceBundle(),"ContentFormatProposal.");
		fContentFormatProposal.setAccelerator(SWT.CTRL | SWT.SHIFT | 'F');
		fContentFormatProposal.setEnabled(true);
		
	}
	
	public void init(IActionBars bars) {
		super.init(bars);
		
		IMenuManager menuManager= bars.getMenuManager();
		IMenuManager editMenu= menuManager.findMenuUsingPath(IWorkbenchActionConstants.M_EDIT);
		if (editMenu != null) {
			editMenu.add(new Separator());
			editMenu.add(fContentAssistProposal);
			editMenu.add(fContentFormatProposal);
		}	
		
	}
	
	private void doSetActiveEditor(IEditorPart part) {
		super.setActiveEditor(part);

		ITextEditor editor= null;
		if (part instanceof ITextEditor)
			editor= (ITextEditor) part;

		fContentAssistProposal.setAction(getAction(editor, "ContentAssistProposal")); //$NON-NLS-1$
		fContentFormatProposal.setAction(getAction(editor, "ContentFormatProposal")); //$NON-NLS-1$
	}
	
	@Override
	public void setActiveEditor(IEditorPart part) {
		super.setActiveEditor(part);
		doSetActiveEditor(part);
	}
	
	
	@Override
	public void dispose() {
		doSetActiveEditor(null);
		super.dispose();
	}

}
