/**
 * 
 */
package org.exlepse.xle.editor.lfg;

import java.util.Iterator;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.projection.ProjectionSupport;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.outline.LfgContentOutlinePage;
import org.exlepse.xle.editor.util.XLEUtils;
import org.exlepse.xle.io.IPrintable;

/**
 * <code>LfgRawEditor</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 6:44:41 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class LfgRawEditor extends TextEditor implements ISelectionChangedListener {

	private final ColorManager colorManager = new ColorManager();

	private ProjectionSupport fProjectionSupport;

	private IEditorInput editorInput;
	private LfgContentOutlinePage outlinePage;

	public LfgRawEditor() {
		setKeyBindingScopes(new String[] { "org.eclipse.ui.textEditorScope" });
		setSourceViewerConfiguration(new LfgSourceViewerConfiguration(colorManager, null));
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		this.editorInput = input;
	}

	@Override
	public void doSave(IProgressMonitor progressMonitor) {
		super.doSave(progressMonitor);

		if (getEditorInput() instanceof FileEditorInput) {

			FileEditorInput editorInput = (FileEditorInput) getEditorInput();
			try {
				XLEUtils.createParser(editorInput.getFile());

				IResource resource = editorInput.getFile();
				resource.deleteMarkers(IMarker.PROBLEM, false, IResource.DEPTH_INFINITE);
			}
			catch (CoreException e) {
				// XLEPlugin.getDefault().getLog().log(e.getStatus());

				try {
					IResource resource = editorInput.getFile();
					IMarker marker = resource.createMarker(IMarker.PROBLEM);
					marker.setAttribute(IMarker.MESSAGE, e.getStatus().getMessage());
					marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
					// marker.setAttribute(IMarker.LOCATION, "line 317");
				}
				catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.editors.text.TextEditor#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object getAdapter(Class adapter) {

		// Object adapter2 = fProjectionSupport.getAdapter(getSourceViewer(), adapter);
		// if (adapter2 != null) return adapter2;

		if (IContentOutlinePage.class.equals(adapter)) {
			if (outlinePage == null) {
				outlinePage = new LfgContentOutlinePage(getDocumentProvider(), this);
				outlinePage.addSelectionChangedListener(this);
			}
			outlinePage.setInput(getEditorInput());
			return outlinePage;
		}
		return super.getAdapter(adapter);
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createSourceViewer(org.eclipse.swt.widgets.Composite,
	// * org.eclipse.jface.text.source.IVerticalRuler, int)
	// */
	// @Override
	// protected ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {
	// return new ProjectionViewer(parent, ruler, fOverviewRuler, true, styles);
	// }
	//
	// /*
	// * (non-Javadoc)
	// *
	// * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createPartControl(org.eclipse.swt.widgets.Composite)
	// */
	// @Override
	// public void createPartControl(Composite parent) {
	// super.createPartControl(parent);
	//
	// ProjectionViewer projectionViewer = (ProjectionViewer) getSourceViewer();
	// fProjectionSupport = new ProjectionSupport(projectionViewer, getAnnotationAccess(), getSharedColors());
	// fProjectionSupport.install();
	// projectionViewer.doOperation(ProjectionViewer.TOGGLE);
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent
	 * )
	 */
	public void selectionChanged(SelectionChangedEvent event) {
		ISelection selection = event.getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			Object o = structuredSelection.getFirstElement();

			if (o instanceof IPrintable) {
				// IPrintable printable = (IPrintable) o;
				// StringDevice device = new StringDevice();
				// try {
				// printable.print(device);
				// }
				// catch (IOException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				if (o != null) {
					String word = o.toString();
					// String word = device.toString();

					IAnnotationModel model = getSourceViewer().getAnnotationModel();
					// Remove old annotations.
					for (Iterator<Annotation> iterator = model.getAnnotationIterator(); iterator.hasNext();) {
						Annotation annotation = iterator.next();
						model.removeAnnotation(annotation);
					}
					IDocument document = getSourceViewer().getDocument();
					createNewAnnotations(word, document, model);
				}
			}
		}
	}

	private void createNewAnnotations(String word, IDocument document, IAnnotationModel model) {
		String content = document.get();
		int idx = content.indexOf(word);
		while (idx != -1) {
			Annotation annotation = new Annotation("org.exlepse.xle.editor.lfg.textMarker", false, word);
			Position position = new Position(idx, word.length());

			model.addAnnotation(annotation, position);
			updateMarkerViews(annotation);
			// fOldAnnotations.add(annotation);

			idx = content.indexOf(word, idx + 1);
		}
	}
}
