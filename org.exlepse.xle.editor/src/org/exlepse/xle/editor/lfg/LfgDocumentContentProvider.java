package org.exlepse.xle.editor.lfg;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.exlepse.xle.XLEDocument;
import org.exlepse.xle.section.ISection;

/**
 * <code>LFGSectionContentProvider</code>.
 * 
 * <pre>
 * Date: Jul 2, 2010
 * Time: 12:23:51 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: LFGSectionContentProvider.java 8133 2010-07-04 13:01:17Z raedle $
 * @since 1.0.0
 */
public class LfgDocumentContentProvider implements ITreeContentProvider, PropertyChangeListener {

	private IDocumentProvider documentProvider;

	/**
	 * @param viewer
	 */
	public LfgDocumentContentProvider(IDocumentProvider documentProvider) {
		this.documentProvider = documentProvider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IStructuredContentProvider#getElements(java.lang.Object)
	 */
	public Object[] getElements(Object inputElement) {
		XLEDocument doc = (XLEDocument) inputElement;
		return doc.getSections().toArray(new ISection[0]);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getChildren(java.lang.Object)
	 */
	public Object[] getChildren(Object parentElement) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	public Object getParent(Object element) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	public boolean hasChildren(Object element) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#dispose()
	 */
	public void dispose() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent arg0) {
		
	}
}
