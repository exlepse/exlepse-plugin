package org.exlepse.xle.editor.lfg.contentassist;

import java.util.ArrayList;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.ITypedRegion;
import org.eclipse.jface.text.contentassist.CompletionProposal;
import org.eclipse.jface.text.contentassist.ICompletionProposal;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContextInformation;
import org.eclipse.jface.text.contentassist.IContextInformationValidator;
import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.ConfigSection;
import org.exlepse.xle.section.SectionHeader;
import org.exlepse.xle.section.SectionHeaderParser;

public class ConfigSectionContentAssistProcessor implements IContentAssistProcessor {

	public ICompletionProposal[] computeCompletionProposals(ITextViewer viewer,
			int offset) {
		try
		{
			IDocument document = viewer.getDocument();
			
			ITypedRegion region = document.getPartition(offset);
	
	        int partitionOffset = region.getOffset();
	        int partitionLength = region.getLength();
	
	        String partitionText = document.get(partitionOffset, partitionLength);
			
	        SectionHeaderParser shParser = new SectionHeaderParser();
	        SectionHeader header = shParser.parse(partitionText.substring(0,partitionText.indexOf('\n')));
	        
	        ConfigParser parser = new ConfigParser(header);
	        ConfigSection section = parser.parse(partitionText.substring(partitionText.indexOf('\n'), partitionText.indexOf("----")));
	        
	        ArrayList<ICompletionProposal> proposals = new ArrayList<ICompletionProposal>();
	        
	        
	        
	        for(String feature : ConfigSection.ConfigFeatures)
	        {
	        	if(!section.hasFeature(feature))
	        	{
	        		proposals.add(new CompletionProposal(feature, offset, 0, feature.length()));
	        	}	
	        }
	        
	        return (ICompletionProposal[])proposals.toArray(new ICompletionProposal[proposals.size()]);
		}
		catch(BadLocationException e)
		{
			
		}
		
		return null;
	}

	public IContextInformation[] computeContextInformation(ITextViewer viewer,
			int offset) {
		// TODO Auto-generated method stub
		return null;
	}

	public char[] getCompletionProposalAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	public char[] getContextInformationAutoActivationCharacters() {
		// TODO Auto-generated method stub
		return null;
	}

	public IContextInformationValidator getContextInformationValidator() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getErrorMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
