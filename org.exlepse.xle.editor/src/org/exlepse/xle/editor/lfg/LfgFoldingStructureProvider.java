/**
 * 
 */
package org.exlepse.xle.editor.lfg;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.text.source.projection.ProjectionAnnotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotationModel;
import org.eclipse.ui.IEditorPart;
import org.exlepse.xle.XLEDocument;

/**
 * <code>LfgFoldingStructureProvider</code>.
 * 
 * <pre>
 * Date: Jul 4, 2010
 * Time: 10:37:31 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class LfgFoldingStructureProvider {

//	private IEditorPart editorPart;
//	
//	public LfgFoldingStructureProvider(IEditorPart editorPart) {
//		this.editorPart = editorPart;
//	}
//	
//	public void updateFoldingRegions(Document document, IProgressMonitor progressMonitor) {
//		ProjectionAnnotationModel model = (ProjectionAnnotationModel) editorPart.getAdapter(ProjectionAnnotationModel.class);
//
//		Set additions = new HashSet();
//		addFoldingRegions(additions, document.getSections());
//
////		Annotation[] deletions = computeDifferences(model, additions);
//
//		Map additionsMap = new HashMap();
//		for (Iterator iter = additions.iterator(); iter.hasNext();)
//			additionsMap.put(new ProjectionAnnotation(), iter.next());
//
////		if ((deletions.length != 0 || additionsMap.size() != 0) && !progressMonitor.isCanceled())
////			model.modifyAnnotations(deletions, additionsMap, new Annotation[] {});
//	}
}
