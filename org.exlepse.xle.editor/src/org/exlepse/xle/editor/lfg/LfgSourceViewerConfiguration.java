/**
 * 
 */
package org.exlepse.xle.editor.lfg;

import org.eclipse.jface.text.DefaultIndentLineAutoEditStrategy;
import org.eclipse.jface.text.IAutoEditStrategy;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.formatter.ContentFormatter;
import org.eclipse.jface.text.formatter.IContentFormatter;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.reconciler.IReconciler;
import org.eclipse.jface.text.reconciler.MonoReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.autoedit.ConfigSectionAutoEditStrategy;
import org.exlepse.xle.editor.lfg.autoedit.LexiconSectionAutoEditStrategy;
import org.exlepse.xle.editor.lfg.autoedit.MorphologySectionAutoEditStrategy;
import org.exlepse.xle.editor.lfg.autoedit.RuleSectionAutoEditStrategy;
import org.exlepse.xle.editor.lfg.autoedit.TemplateSectionAutoEditStrategy;
import org.exlepse.xle.editor.lfg.contentassist.ConfigSectionContentAssistProcessor;
import org.exlepse.xle.editor.lfg.contentassist.LexiconSectionContentAssistProcessor;
import org.exlepse.xle.editor.lfg.contentassist.MorphologySectionContentAssistProcessor;
import org.exlepse.xle.editor.lfg.contentassist.RuleSectionContentAssistProcessor;
import org.exlepse.xle.editor.lfg.contentassist.TemplateSectionContentAssistProcessor;
import org.exlepse.xle.editor.lfg.formatter.ConfigSectionFormattingStrategy;
import org.exlepse.xle.editor.lfg.formatter.LexiconSectionFormattingStrategy;
import org.exlepse.xle.editor.lfg.formatter.MorphologySectionFormattingStrategy;
import org.exlepse.xle.editor.lfg.formatter.RuleSectionFormattingStrategy;
import org.exlepse.xle.editor.lfg.formatter.TemplateSectionFormattingStrategy;
import org.exlepse.xle.editor.lfg.scanner.ConfigSectionScanner;
import org.exlepse.xle.editor.lfg.scanner.LexiconEntryTokenScanner;
import org.exlepse.xle.editor.lfg.scanner.LexiconSectionScanner;
import org.exlepse.xle.editor.lfg.scanner.LfgPartitionScanner;
import org.exlepse.xle.editor.lfg.scanner.MorphologySectionScanner;
import org.exlepse.xle.editor.lfg.scanner.RuleScanner;
import org.exlepse.xle.editor.lfg.scanner.RuleSectionScanner;
import org.exlepse.xle.editor.lfg.scanner.TemplateScanner;
import org.exlepse.xle.editor.lfg.scanner.TemplateSectionScanner;

/**
 * <code>LfgSourceViewerConfiguration</code>.
 * 
 * <pre>
 * Date: Jul 4, 2010
 * Time: 7:10:33 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class LfgSourceViewerConfiguration extends SourceViewerConfiguration {

	private ColorManager colorManager = new ColorManager();

	private ConfigSectionScanner configSectionScanner;
	private RuleSectionScanner ruleSectionScanner;
	private RuleScanner ruleScanner;
	private LexiconSectionScanner lexiconSectionScanner;
	private LexiconEntryTokenScanner lexiconEntryTokenScanner;
	private MorphologySectionScanner morphologySectionScanner;
	private TemplateSectionScanner templateSectionScanner;
	private TemplateScanner templateScanner;
	private LfgEditor editor;
	
	public LfgSourceViewerConfiguration(ColorManager colorManager, LfgEditor editor) {
		this.colorManager = colorManager;
		this.editor = editor;
	}
	
	@Override
    public IAutoEditStrategy[] getAutoEditStrategies(ISourceViewer sourceViewer, String contentType) {
        
		IAutoEditStrategy strategy = new DefaultIndentLineAutoEditStrategy();
		
		if(contentType.equals(LfgPartitionScanner.CONFIG_SECTION))
		{
			strategy = new ConfigSectionAutoEditStrategy();
		}
		else if(contentType.equals(LfgPartitionScanner.LEXICON_SECTION))
		{
			strategy = new LexiconSectionAutoEditStrategy();
		}	
		else if(contentType.equals(LfgPartitionScanner.MORPHOLOGY_SECTION))
		{
			strategy = new MorphologySectionAutoEditStrategy();
		}		
		else if(contentType.equals(LfgPartitionScanner.RULE_SECTION))
		{
			strategy = new RuleSectionAutoEditStrategy();
		}			
		else if(contentType.equals(LfgPartitionScanner.TEMPLATE_SECTION))
		{
			strategy = new TemplateSectionAutoEditStrategy();
		}	
		
		return new IAutoEditStrategy[] {strategy};
    } 
	
	@Override
	public IContentAssistant getContentAssistant(ISourceViewer sourceViewer)
    {
		ContentAssistant assistant = new ContentAssistant();

        assistant.setContentAssistProcessor(new ConfigSectionContentAssistProcessor(),LfgPartitionScanner.CONFIG_SECTION);
        assistant.setContentAssistProcessor(new LexiconSectionContentAssistProcessor(),LfgPartitionScanner.LEXICON_SECTION);
        assistant.setContentAssistProcessor(new MorphologySectionContentAssistProcessor(),LfgPartitionScanner.MORPHOLOGY_SECTION);
        assistant.setContentAssistProcessor(new RuleSectionContentAssistProcessor(),LfgPartitionScanner.RULE_SECTION);
        assistant.setContentAssistProcessor(new TemplateSectionContentAssistProcessor(),LfgPartitionScanner.TEMPLATE_SECTION);
        assistant.enableAutoActivation(true);
        assistant.setAutoActivationDelay(500);
        assistant.setProposalPopupOrientation(IContentAssistant.CONTEXT_INFO_BELOW);
        assistant.setContextInformationPopupOrientation(IContentAssistant.CONTEXT_INFO_BELOW);
        return assistant;

    }
	
	
	@Override
	public IContentFormatter getContentFormatter(ISourceViewer sourceViewer)
    {
        ContentFormatter formatter = new ContentFormatter();
        
        formatter.enablePartitionAwareFormatting(true);
        
        
        ConfigSectionFormattingStrategy configStrategy = new ConfigSectionFormattingStrategy();
        LexiconSectionFormattingStrategy lexiconStrategy = new LexiconSectionFormattingStrategy();
        MorphologySectionFormattingStrategy morphologyStrategy = new MorphologySectionFormattingStrategy();
        RuleSectionFormattingStrategy ruleStrategy = new RuleSectionFormattingStrategy();
        TemplateSectionFormattingStrategy templateStrategy = new TemplateSectionFormattingStrategy();
        
        formatter.setFormattingStrategy(configStrategy, LfgPartitionScanner.CONFIG_SECTION);
        formatter.setFormattingStrategy(lexiconStrategy, LfgPartitionScanner.LEXICON_SECTION);
        formatter.setFormattingStrategy(morphologyStrategy, LfgPartitionScanner.MORPHOLOGY_SECTION);
        formatter.setFormattingStrategy(ruleStrategy, LfgPartitionScanner.RULE_SECTION);
        formatter.setFormattingStrategy(templateStrategy, LfgPartitionScanner.TEMPLATE_SECTION);
        
        return formatter;
    }
	
	@Override
	public String[] getConfiguredContentTypes(ISourceViewer sourceViewer)
	{	
		return LfgPartitionScanner.PARTITION_TYPES;	
	}
	
	protected ConfigSectionScanner getConfigSectionScanner()
    {
        if (configSectionScanner == null)
        {
        	configSectionScanner = new ConfigSectionScanner(colorManager);
        }
        return configSectionScanner;
    }

    protected RuleSectionScanner getRuleSectionScanner()
    {
        if (ruleSectionScanner == null)
        {
        	ruleSectionScanner = new RuleSectionScanner(colorManager);
        }
        return ruleSectionScanner;
    }
    
    protected RuleScanner getRuleScanner()
    {
        if (ruleScanner == null)
        {
        	ruleScanner = new RuleScanner(colorManager);
        }
        return ruleScanner;
    }

    protected LexiconSectionScanner getLexiconSectionScanner()
    {
    	if (lexiconSectionScanner == null)
        {
        	lexiconSectionScanner = new LexiconSectionScanner(colorManager);
        }
        return lexiconSectionScanner;
    }
    
    protected LexiconEntryTokenScanner getLexiconEntryTokenScanner()
    {
    	if (lexiconEntryTokenScanner == null)
        {
    		lexiconEntryTokenScanner = new LexiconEntryTokenScanner(colorManager);
        }
        return lexiconEntryTokenScanner;
    }

    protected MorphologySectionScanner getMorphologySectionScanner()
    {
        if (morphologySectionScanner == null)
        {
        	morphologySectionScanner = new MorphologySectionScanner(colorManager);
        }
        return morphologySectionScanner;
    }
    
    protected TemplateSectionScanner getTemplateSectionScanner()
    {
        if (templateSectionScanner == null)
        {
        	templateSectionScanner = new TemplateSectionScanner(colorManager);
        }
        return templateSectionScanner;
    }
    
    protected TemplateScanner getTemplateScanner()
    {
        if (templateScanner == null)
        {
        	templateScanner = new TemplateScanner(colorManager);
        }
        return templateScanner;
    }

    public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer)
    {
        PresentationReconciler reconciler = new PresentationReconciler();

        DefaultDamagerRepairer dr = new DefaultDamagerRepairer(getConfigSectionScanner());
        reconciler.setDamager(dr, LfgPartitionScanner.CONFIG_SECTION);
        reconciler.setRepairer(dr, LfgPartitionScanner.CONFIG_SECTION);

        dr = new DefaultDamagerRepairer(getLexiconEntryTokenScanner());
        reconciler.setDamager(dr, LfgPartitionScanner.LEXICON_SECTION);
        reconciler.setRepairer(dr, LfgPartitionScanner.LEXICON_SECTION);

        dr = new DefaultDamagerRepairer(getMorphologySectionScanner());
        reconciler.setDamager(dr, LfgPartitionScanner.MORPHOLOGY_SECTION);
        reconciler.setRepairer(dr, LfgPartitionScanner.MORPHOLOGY_SECTION);

        dr = new DefaultDamagerRepairer(getRuleScanner());
        reconciler.setDamager(dr, LfgPartitionScanner.RULE_SECTION);
        reconciler.setRepairer(dr, LfgPartitionScanner.RULE_SECTION);

        dr = new DefaultDamagerRepairer(getTemplateScanner());
        reconciler.setDamager(dr, LfgPartitionScanner.TEMPLATE_SECTION);
        reconciler.setRepairer(dr, LfgPartitionScanner.TEMPLATE_SECTION);

        return reconciler;
    }
	
    public IReconciler getReconciler(ISourceViewer sourceViewer)
    {
        LfgReconcilingStrategy strategy = new LfgReconcilingStrategy();
        strategy.setEditor(editor);
        
        MonoReconciler reconciler = new MonoReconciler(strategy,false);
        
        return reconciler;
    }
	
}
