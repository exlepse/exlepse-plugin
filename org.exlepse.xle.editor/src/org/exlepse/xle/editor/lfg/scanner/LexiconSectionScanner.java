package org.exlepse.xle.editor.lfg.scanner;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.MultiLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

public class LexiconSectionScanner extends LfgSectionScanner {

	public LexiconSectionScanner(ColorManager manager)
	{
		super(manager);
		
		IToken wordToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.LEXICON_WORD_COLOR), null, SWT.NORMAL , null));
		
		IRule[] rules = new IRule[5];
		
		rules[0] = commentRule;
		rules[1] = getSectionHeaderRule("LEXICON");
		
		
		MultiLineRule lexEntryRule = new MultiLineRule("\n", ".", commentToken);
		rules[2] = lexEntryRule;
		
		rules[3] = sectionFooterRule;
		rules[4] = whitespaceRule;
		
		setDefaultReturnToken(wordToken);
		
		setRules(rules);
	}
	
}
