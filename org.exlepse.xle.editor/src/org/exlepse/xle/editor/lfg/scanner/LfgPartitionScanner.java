package org.exlepse.xle.editor.lfg.scanner;


import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.rules.*;
import org.exlepse.xle.editor.lfg.rules.ConfigSectionRule;
import org.exlepse.xle.editor.lfg.rules.LexiconSectionRule;
import org.exlepse.xle.editor.lfg.rules.MorphologySectionRule;
import org.exlepse.xle.editor.lfg.rules.NonMatchingRule;
import org.exlepse.xle.editor.lfg.rules.RuleSectionRule;
import org.exlepse.xle.editor.lfg.rules.TemplateSectionRule;

public class LfgPartitionScanner extends RuleBasedPartitionScanner {
	
	
	public static final String CONFIG_SECTION = "configSection";
	public static final String RULE_SECTION = "ruleSection";
	public static final String LEXICON_SECTION = "lexiconSection";
	public static final String TEMPLATE_SECTION = "templateSection";
	public static final String MORPHOLOGY_SECTION = "morphologySection";
	
	public static final String[] PARTITION_TYPES = {IDocument.DEFAULT_CONTENT_TYPE, CONFIG_SECTION, RULE_SECTION, LEXICON_SECTION, TEMPLATE_SECTION, MORPHOLOGY_SECTION};
	
	public LfgPartitionScanner()
	{
		IToken configSectionToken = new Token(LfgPartitionScanner.CONFIG_SECTION);
		IToken ruleSectionToken = new Token(LfgPartitionScanner.RULE_SECTION);
		IToken lexiconSectionToken = new Token(LfgPartitionScanner.LEXICON_SECTION);
		IToken templateSectionToken = new Token(LfgPartitionScanner.TEMPLATE_SECTION);
		IToken morphologySectionToken = new Token(LfgPartitionScanner.MORPHOLOGY_SECTION);
		
		IPredicateRule[] rules = new IPredicateRule[6];
		
		rules[0] = new NonMatchingRule();
		rules[1] = new ConfigSectionRule(configSectionToken);
		rules[2] = new RuleSectionRule(ruleSectionToken);
		rules[3] = new LexiconSectionRule(lexiconSectionToken);
		rules[4] = new TemplateSectionRule(templateSectionToken);
		rules[5] = new MorphologySectionRule(morphologySectionToken);
		
		setPredicateRules(rules);
	}

}
