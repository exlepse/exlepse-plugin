package org.exlepse.xle.editor.lfg.scanner;

import java.util.ArrayList;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.swt.SWT;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;
import org.exlepse.xle.editor.lfg.rules.LfgWhitespaceDetector;
import org.exlepse.xle.editor.lfg.rules.RegExRule;

public class LfgSectionScanner extends RuleBasedScanner {

	protected IRule commentRule;
	protected IRule sectionFooterRule;
	protected IRule whitespaceRule;
	
	protected IToken commentToken;
	protected IToken sectionHeaderToken;
	protected IToken sectionFooterToken;
	protected IToken defaultToken;
	
	protected ArrayList<IRule> rules;
	
	public LfgSectionScanner(ColorManager manager)
	{
		rules = new ArrayList<IRule>();
		
		commentToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.COMMENT_COLOR), null, SWT.ITALIC, null));
		sectionHeaderToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.SECTION_HEADER_COLOR), null, SWT.BOLD, null));
		sectionFooterToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.SECTION_HEADER_COLOR), null, SWT.BOLD, null));
		defaultToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.DEFAULT_COLOR), null, SWT.NORMAL, null));
		
		commentRule = new SingleLineRule("\"", "\"", commentToken);
		sectionFooterRule = new RegExRule("-{4}", sectionFooterToken);
		whitespaceRule = new WhitespaceRule(new LfgWhitespaceDetector());
		
		setDefaultReturnToken(defaultToken);
	}
	
	protected IRule getSectionHeaderRule(String sectionHeader)
	{
		return new RegExRule("(\\w+)\\s+(\\w+)\\s+(" + sectionHeader + ")\\s+(\\(1\\.0\\))", sectionHeaderToken);
	}
	
	protected void setRules(ArrayList<IRule> rules)
	{
		setRules((IRule[])rules.toArray(new IRule[rules.size()]));
	}
	
}
