package org.exlepse.xle.editor.lfg.scanner;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

public class RuleSectionScanner extends LfgSectionScanner {

	public RuleSectionScanner(ColorManager manager)
	{
		super(manager);
		
		IToken sourceToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.RULE_SOURCE_COLOR), null, SWT.NORMAL , null));
		
		IRule[] rules = new IRule[4];
		
		rules[0] = commentRule;
		rules[1] = getSectionHeaderRule("RULES");
		rules[2] = sectionFooterRule;
		rules[3] = whitespaceRule;
		
		setDefaultReturnToken(sourceToken);
		
		setRules(rules);
	}
	
}
