package org.exlepse.xle.editor.lfg.scanner;

import org.eclipse.jface.text.rules.IRule;
import org.exlepse.xle.editor.common.ColorManager;

public class MorphologySectionScanner extends LfgSectionScanner {

	public MorphologySectionScanner(ColorManager manager)
	{
		super(manager);
		
		IRule[] rules = new IRule[4];
		
		rules[0] = commentRule;
		rules[1] = getSectionHeaderRule("MORPHOLOGY");
		rules[2] = sectionFooterRule;
		rules[3] = whitespaceRule;
		
		setRules(rules);
	}
	
}
