package org.exlepse.xle.editor.lfg.scanner;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.swt.SWT;
import org.exlepse.xle.config.ConfigSection;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;
import org.exlepse.xle.editor.lfg.rules.ConfigSectionKeyWordDetector;
import org.exlepse.xle.editor.lfg.rules.RegExRule;

public class ConfigSectionScanner extends LfgSectionScanner {

	public ConfigSectionScanner(ColorManager manager)
	{
		super(manager);
		
		IToken keyToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.CONFIG_KEY_COLOR), null, SWT.NORMAL , null));
		IToken singleValueToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.CONFIG_SINGLE_VALUE_COLOR), null, SWT.NORMAL , null));
		IToken sectionValueToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.CONFIG_SECTION_VALUE_COLOR), null, SWT.NORMAL , null));
		IToken pointToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.DEFAULT_COLOR), null, SWT.BOLD , null));
		
		WordRule keywordRule = new WordRule(new ConfigSectionKeyWordDetector());
		for(int i=0; i<ConfigSection.ConfigFeatures.length; i++)
		{
			keywordRule.addWord(ConfigSection.ConfigFeatures[i], keyToken);
		}
		
		rules.add(commentRule);
		rules.add(new RegExRule("\\.", pointToken));
		rules.add(getSectionHeaderRule("CONFIG"));
		rules.add(keywordRule);
		rules.add(new SingleLineRule("(", ")", sectionValueToken));
		rules.add(sectionFooterRule);
		rules.add(whitespaceRule);
		
		setDefaultReturnToken(singleValueToken);
		
		setRules(rules);
		
	}
}
