package org.exlepse.xle.editor.lfg.scanner;

import java.util.HashMap;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

public class RuleScanner extends LfgSectionScanner {

	private IToken sourceToken;
	private IToken arrowToken;
	private IToken targetToken;
	
	public RuleScanner(ColorManager manager)
	{
		super(manager);
		tokens = new HashMap<Integer, Object[]>();
		sourceToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.RULE_SOURCE_COLOR), null, SWT.NORMAL , null));
		arrowToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.RULE_ARROW_COLOR), null, SWT.BOLD , null));
		targetToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.RULE_TARGET_COLOR), null, SWT.NORMAL , null));
	}
	
	
	private boolean headerRead = false;
	private HashMap<Integer, Object[]> tokens;
	
	@Override
	public IToken nextToken() {
		
		fTokenOffset = fOffset;
		
		if(!headerRead)
		{
			IRule sectionHeaderRule = getSectionHeaderRule("RULES");
			
			IToken match = sectionHeaderRule.evaluate(this);
			
			headerRead = true;
			
			return match;
		}
		
		if(tokens.containsKey(fOffset))
		{
			Object[] data = tokens.get(fOffset);
			tokens.remove(fOffset);
			IToken token = (IToken) data[0];
			fOffset += (Integer) data[1];
			
			return token;
		}
		
		int offsetSave = fOffset;
		
		int c = read();
		
		if(Character.isWhitespace(c))
			return Token.WHITESPACE;
		
		if (c == EOF)
		{
			headerRead = false;
			tokens = new HashMap<Integer, Object[]>();
			return Token.EOF;	
		}
		
		
		StringBuffer buffer = new StringBuffer();
		//we can safely append the first read character, since we know it must be some valid character
		buffer.append((char)c);
		c = read();
		
		String source = null;
		String arrow = null;
		String target = null;

		while (c != EOF && c != '.')
		{
            buffer.append((char) c);
            c = read();
        }
		
		if(c == EOF)
		{
			unread();
		}
		else
		{
			buffer.append((char)c);
		}
		
		String entry = buffer.toString();
		buffer = new StringBuffer();
		char[] chars = entry.toCharArray();
		
		fOffset = offsetSave;
		
		int i=0;
		char currentChar = ' ';		
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(!Character.isWhitespace(currentChar))
			{
				buffer.append(currentChar);
				i++;
			}
			else
			{
				break;
			}
		}
		
		source = buffer.toString();
		
		if(source.equals("----"))
		{
			tokens.put(offsetSave, new Object[]{sectionFooterToken, 4});
			return Token.UNDEFINED;
		}
		
		buffer = new StringBuffer();
		tokens.put(offsetSave, new Object[]{sourceToken, source.length()});
		offsetSave += source.length();
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(Character.isWhitespace(currentChar))
			{
				offsetSave++;
				i++;
			}
			else
			{
				break;
			}
		}
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(!Character.isWhitespace(currentChar))
			{
				buffer.append(currentChar);
				i++;
			}
			else
			{
				break;
			}
		}
		
		arrow = buffer.toString();
		buffer = new StringBuffer();
		tokens.put(offsetSave, new Object[]{arrowToken, arrow.length()});
		offsetSave += arrow.length();
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(Character.isWhitespace(currentChar))
			{
				offsetSave++;
				i++;
			}
			else
			{
				break;
			}
		}
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			buffer.append(currentChar);
			i++;
		}
		
		target = buffer.toString();
		buffer = new StringBuffer();
		tokens.put(offsetSave, new Object[]{targetToken, target.length()});
		
		return Token.UNDEFINED;
		
	}
	
}
