package org.exlepse.xle.editor.lfg.scanner;

import java.util.HashMap;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

public class TemplateScanner extends LfgSectionScanner {

	private IToken templateNameToken;
	private IToken equalSignToken;
	private IToken templateBodyToken;
	
	private boolean headerRead = false;
	private HashMap<Integer, Object[]> tokens;
	
	public TemplateScanner(ColorManager manager)
	{
		super(manager);
		tokens = new HashMap<Integer, Object[]>();
		templateNameToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.TEMPLATE_NAME_COLOR), null, SWT.NORMAL , null));
		equalSignToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.TEMPLATE_EQUALSIGN_COLOR), null, SWT.BOLD , null));
		templateBodyToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.TEMPLATE_BODY_COLOR), null, SWT.NORMAL , null));
	}
	
	@Override
	public IToken nextToken() {

fTokenOffset = fOffset;
		
		if(!headerRead)
		{
			IRule sectionHeaderRule = getSectionHeaderRule("TEMPLATES");
			
			IToken match = sectionHeaderRule.evaluate(this);
			
			headerRead = true;
			
			return match;
		}
		
		if(tokens.containsKey(fOffset))
		{
			Object[] data = tokens.get(fOffset);
			tokens.remove(fOffset);
			IToken token = (IToken) data[0];
			fOffset += (Integer) data[1];
			
			return token;
		}
		
		int offsetSave = fOffset;
		
		int c = read();
		
		if(Character.isWhitespace(c))
			return Token.WHITESPACE;
		
		if (c == EOF)
		{
			headerRead = false;
			tokens = new HashMap<Integer, Object[]>();
			return Token.EOF;	
		}
		
		
		StringBuffer buffer = new StringBuffer();
		//we can safely append the first read character, since we know it must be some valid character
		buffer.append((char)c);
		c = read();
		
		String templateName = null;
		String equalSign = null;
		String templateBody = null;

		while (c != EOF && c != '.')
		{
            buffer.append((char) c);
            c = read();
        }
		
		if(c == EOF)
		{
			unread();
		}
		else
		{
			buffer.append((char)c);
		}
		
		String entry = buffer.toString();
		buffer = new StringBuffer();
		char[] chars = entry.toCharArray();
		
		fOffset = offsetSave;
		
		int i=0;
		char currentChar = ' ';		
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(!(currentChar == '='))
			{
				buffer.append(currentChar);
				i++;
			}
			else
			{
				break;
			}
		}
		
		templateName = buffer.toString();
		
		if(templateName.equals("----"))
		{
			tokens.put(offsetSave, new Object[]{sectionFooterToken, 4});
			return Token.UNDEFINED;
		}
		
		buffer = new StringBuffer();
		tokens.put(offsetSave, new Object[]{templateNameToken, templateName.length()});
		offsetSave += templateName.length();
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(Character.isWhitespace(currentChar))
			{
				offsetSave++;
				i++;
			}
			else
			{
				break;
			}
		}
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(!Character.isWhitespace(currentChar))
			{
				buffer.append(currentChar);
				i++;
			}
			else
			{
				break;
			}
		}
		
		equalSign = buffer.toString();
		buffer = new StringBuffer();
		tokens.put(offsetSave, new Object[]{equalSignToken, equalSign.length()});
		offsetSave += equalSign.length();
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(Character.isWhitespace(currentChar))
			{
				offsetSave++;
				i++;
			}
			else
			{
				break;
			}
		}
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			buffer.append(currentChar);
			i++;
		}
		
		templateBody = buffer.toString();
		buffer = new StringBuffer();
		tokens.put(offsetSave, new Object[]{templateBodyToken, templateBody.length()});
		
		return Token.UNDEFINED;
		
	}
	
	
}
