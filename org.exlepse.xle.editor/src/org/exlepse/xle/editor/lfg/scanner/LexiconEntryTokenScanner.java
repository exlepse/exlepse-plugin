package org.exlepse.xle.editor.lfg.scanner;

import java.util.HashMap;
import java.util.Scanner;

import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.ITokenScanner;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.swt.SWT;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.lfg.preferences.XLEPreferences;

public class LexiconEntryTokenScanner extends LfgSectionScanner implements ITokenScanner {

	
	private IToken wordToken;
	private IToken partOfSpeechToken;
	private IToken morphCodeToken;
	private IToken schemataToken;
	
	public LexiconEntryTokenScanner(ColorManager manager)
	{
		super(manager);
		tokens = new HashMap<Integer, Object[]>();
		wordToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.LEXICON_WORD_COLOR), null, SWT.NORMAL , null));
		partOfSpeechToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.LEXICON_PARTOFSPEECH_COLOR), null, SWT.NORMAL , null));
		morphCodeToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.LEXICON_MORPHCODE_COLOR), null, SWT.NORMAL , null));
		schemataToken = new Token(new TextAttribute(manager.getColor(XLEPreferences.LEXICON_SCHEMATA_COLOR), null, SWT.NORMAL , null));
	}
	
	private HashMap<Integer, Object[]> tokens;
	
	private boolean headerRead = false;
	
	@Override
	public IToken nextToken() {
		
		fTokenOffset = fOffset;
	
		if(!headerRead)
		{
			IRule sectionHeaderRule = getSectionHeaderRule("LEXICON");
			
			IToken match = sectionHeaderRule.evaluate(this);
			
			headerRead = true;
			
			return match;
		}
		
		if(tokens.containsKey(fOffset))
		{
			Object[] data = tokens.get(fOffset);
			tokens.remove(fOffset);
			IToken token = (IToken) data[0];
			fOffset += (Integer) data[1];
			
			return token;
		}
		
		int offsetSave = fOffset;
		
		int c = read();
		
		if(Character.isWhitespace(c))
			return Token.WHITESPACE;
		
		if (c == EOF)
		{
			headerRead = false;
			tokens = new HashMap<Integer, Object[]>();
			return Token.EOF;	
		}
		
		StringBuffer buffer = new StringBuffer();
		//we can safely append the first read character, since we know it must be some valid character
		//this allows the special point (.) lex entry to pass through
		buffer.append((char)c);
		c = read();
		
		String word = null;

		while (c != EOF && c != '.')
		{
            buffer.append((char) c);
            c = read();
        }
		
		if(c == EOF)
		{
			unread();
		}
		else
		{
			buffer.append((char)c);
		}
		
		String entry = buffer.toString();
		buffer = new StringBuffer();
		char[] chars = entry.toCharArray();
		
		fOffset = offsetSave;
		
		int i=0;
		char currentChar = ' ';		
		
		while(i < chars.length)
		{
			currentChar = chars[i];
			if(!Character.isWhitespace(currentChar))
			{
				buffer.append(currentChar);
				i++;
			}
			else
			{
				break;
			}
		}
		
		word = buffer.toString();
		
		if(word.equals("----"))
		{
			tokens.put(offsetSave, new Object[]{sectionFooterToken, 4});
			return Token.UNDEFINED;
		}
		
		buffer = new StringBuffer();
		tokens.put(offsetSave, new Object[]{wordToken, word.length()});
		offsetSave += word.length();
		
		entry = entry.substring(i);
			
		Scanner scanner = new Scanner(entry);
		scanner.useDelimiter(";");
			
		while(scanner.hasNext())
		{
			String part = scanner.next();
			String partOfSpeech = null;
			String morphCode = null;
			String schemata = null;
			buffer = new StringBuffer();
			
			chars = part.toCharArray();
			i=0;
			
			while(i < chars.length)
			{
				currentChar = chars[i];
				if(Character.isWhitespace(currentChar))
				{
					offsetSave++;
					i++;
				}
				else
				{
					break;
				}
			}
			
			while(i < chars.length)
			{
				currentChar = chars[i];
				if(!Character.isWhitespace(currentChar))
				{
					buffer.append(currentChar);
					i++;
				}
				else
				{
					break;
				}
			}
			
			partOfSpeech = buffer.toString();
			buffer = new StringBuffer();
			tokens.put(offsetSave, new Object[]{partOfSpeechToken, partOfSpeech.length()});
			offsetSave += partOfSpeech.length();
			
			while(i < chars.length)
			{
				currentChar = chars[i];
				if(Character.isWhitespace(currentChar))
				{
					offsetSave++;
					i++;
				}
				else
				{
					break;
				}
			}
			
			while(i < chars.length)
			{
				currentChar = chars[i];
				if(!Character.isWhitespace(currentChar))
				{
					buffer.append(currentChar);
					i++;
				}
				else
				{
					break;
				}
			}
			

			morphCode = buffer.toString();
			buffer = new StringBuffer();
			tokens.put(offsetSave, new Object[]{morphCodeToken, morphCode.length()});
			
			offsetSave += morphCode.length();

			while(i < chars.length)
			{
				currentChar = chars[i];
				if(Character.isWhitespace(currentChar))
				{
					offsetSave++;
					i++;
				}
				else
				{
					break;
				}
			}
			
			while(i < chars.length)
			{
				currentChar = chars[i];
				buffer.append(currentChar);
				i++;
			}

			schemata = buffer.toString();
			
			tokens.put(offsetSave, new Object[]{schemataToken, schemata.length()});
		}
		
		return Token.UNDEFINED;		
	}

}
