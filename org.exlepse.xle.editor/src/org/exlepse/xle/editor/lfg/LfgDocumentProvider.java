/**
 * 
 */
package org.exlepse.xle.editor.lfg;

import java.io.ByteArrayInputStream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IDocumentPartitioner;
import org.eclipse.jface.text.source.AnnotationModel;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.ui.editors.text.FileDocumentProvider;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.exlepse.xle.DocumentParser;
import org.exlepse.xle.editor.lfg.scanner.LfgPartitionScanner;

/**
 * <code>LfgDocumentProvider</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 4:44:43 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class LfgDocumentProvider extends FileDocumentProvider implements IDocumentProvider {

	private DocumentParser documentParser = new DocumentParser();

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#createAnnotationModel(java.lang.Object)
	 */
	@Override
	protected IAnnotationModel createAnnotationModel(Object element) throws CoreException {
		return new AnnotationModel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#createDocument(java.lang.Object)
	 */
	@Override
	protected IDocument createDocument(Object element) throws CoreException {

		if (element instanceof FileEditorInput) {
			
			//File file = ((FileEditorInput) element).getPath().toFile();
			
			//String charset = ((FileEditorInput)element).getFile().getCharset();
			
			
			
//			XLEDocument document = null;
//			try {
//				document = documentParser.parse(file);
//			}
//			catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//			StringDevice device = new StringDevice();
//			try {
//				document.print(device);
//			}
//			catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
			
			IDocument doc = super.createDocument(element);

			LfgPartitionScanner scanner = new LfgPartitionScanner();
			IDocumentPartitioner partitioner = new LfgPartitioner(scanner, LfgPartitionScanner.PARTITION_TYPES);
			partitioner.connect(doc);
			doc.setDocumentPartitioner(partitioner);			
			
			return doc;
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#isReadOnly(java.lang.Object)
	 */
	@Override
	public boolean isReadOnly(Object element) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#isModifiable(java.lang.Object)
	 */
	@Override
	public boolean isModifiable(Object element) {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.texteditor.AbstractDocumentProvider#doSaveDocument(org.eclipse.core.runtime.IProgressMonitor,
	 * java.lang.Object, org.eclipse.jface.text.IDocument, boolean)
	 */
	@Override
	protected void doSaveDocument(IProgressMonitor monitor, Object element, IDocument document, boolean overwrite)
			throws CoreException {

		if (element instanceof FileEditorInput) {
//			monitor.beginTask("Saving XLE document", 1);

			try {
				((FileEditorInput) element).getFile().setContents(new ByteArrayInputStream(document.get().getBytes()), true, false, monitor);
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
//			monitor.done();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.ui.texteditor.AbstractDocumentProvider#getOperationRunner(org.eclipse.core.runtime.IProgressMonitor)
	 */
	@Override
	protected IRunnableContext getOperationRunner(IProgressMonitor monitor) {
		// TODO Auto-generated method stub
		return null;
	}

}
