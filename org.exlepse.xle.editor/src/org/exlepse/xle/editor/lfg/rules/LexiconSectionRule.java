package org.exlepse.xle.editor.lfg.rules;

import org.eclipse.jface.text.rules.IToken;

public class LexiconSectionRule extends LfgSectionRule {
	
	public LexiconSectionRule(IToken token)
	{
		super(token, "LEXICON");
		//super("(\\w+)\\s+(\\w+)\\s+(LEXICON)\\s+(\\(1\\.0\\))(.*)(----)", token);
	}
	
}
