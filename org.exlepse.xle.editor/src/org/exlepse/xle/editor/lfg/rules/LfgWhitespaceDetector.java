package org.exlepse.xle.editor.lfg.rules;

import org.eclipse.jface.text.rules.IWhitespaceDetector;

public class LfgWhitespaceDetector implements IWhitespaceDetector {

	public boolean isWhitespace(char c) {
		return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
	}

}
