package org.exlepse.xle.editor.lfg.rules;

import org.eclipse.jface.text.rules.IToken;

public class RuleSectionRule extends LfgSectionRule {

	public RuleSectionRule(IToken token)
	{
		super(token,"RULES");
		//super("(\\w+)\\s+(\\w+)\\s+(RULES)\\s+(\\(1\\.0\\))(.*)(----)", token);
	}
	
}
