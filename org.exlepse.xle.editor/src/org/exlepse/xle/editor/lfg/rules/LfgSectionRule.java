package org.exlepse.xle.editor.lfg.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IPredicateRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

public class LfgSectionRule implements IPredicateRule {

	private IToken token;
	private String sectionName;
	
	public LfgSectionRule(IToken token, String sectionName)
	{
		this.token = token;
		this.sectionName = sectionName;
	}
	
	

	public IToken getSuccessToken() {
		return token;
	}

	public IToken evaluate(ICharacterScanner scanner) {
		return evaluate(scanner, false);
	}



	public IToken evaluate(ICharacterScanner scanner, boolean resume) {
		
		CharacterScannerCharSequence charSequence = new CharacterScannerCharSequence(scanner);
		
		Pattern pattern = Pattern.compile("(\\w+)\\s+(\\w+)\\s+(" + sectionName + ")\\s+(\\(1\\.0\\))");
		Matcher matcher = pattern.matcher(charSequence);
		
		if(matcher.lookingAt())
		{
			int headerEndPosition = matcher.end();
			
			boolean endState = false;
			int endCounter = 0;
			
			int i=0;
			
			for(i=headerEndPosition; i<charSequence.length(); i++)
			{
				char current = charSequence.charAt(i);
				boolean match = (current == '-');
				
				
				if(endState && endCounter == 4)
				{
					//skip all following whitespaces or break when next section is reached
					if(Character.isWhitespace(current))
						continue;
					else
						break;
				}
				
				
				
				
				if(endState && match)
				{
					endCounter++;
				}
				else if(endState && !match)
				{
					endCounter = 0;
					endState = false;
				}
				else if(!endState && match)
				{
					endState = true;
					endCounter++;
				}
			}
			
			charSequence.unread(charSequence.length() - i);
			return getSuccessToken();
		}
		
		charSequence.unread();
		return Token.UNDEFINED;
	}

}
