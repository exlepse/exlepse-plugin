package org.exlepse.xle.editor.lfg.rules;

import org.eclipse.jface.text.rules.IToken;

public class MorphologySectionRule extends LfgSectionRule {

	public MorphologySectionRule(IToken token)
	{
		super(token,"MORPHOLOGY");
		//super("(\\w+)\\s+(\\w+)\\s+(MORPHOLOGY)\\s+(\\(1\\.0\\))(.*)(----)", token);
	}
	
}
