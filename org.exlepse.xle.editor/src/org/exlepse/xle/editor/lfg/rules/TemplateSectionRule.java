package org.exlepse.xle.editor.lfg.rules;

import org.eclipse.jface.text.rules.IToken;

public class TemplateSectionRule extends LfgSectionRule{

	public TemplateSectionRule(IToken token)
	{
		super(token,"TEMPLATES");
		//super("(\\w+)\\s+(\\w+)\\s+(TEMPLATES)\\s+(\\(1\\.0\\))(.*)(----)", token);
	}
	
}
