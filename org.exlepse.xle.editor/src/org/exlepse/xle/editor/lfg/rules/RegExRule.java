package org.exlepse.xle.editor.lfg.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.jface.text.rules.ICharacterScanner;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.Token;

public class RegExRule implements IRule
{
    private Pattern pattern;
    private IToken token;

    public RegExRule(String patternString, IToken token)
    {
        pattern = Pattern.compile(patternString);
        this.token = token;
    }

    public IToken evaluate(ICharacterScanner scanner)
    {
    	CharacterScannerCharSequence charSequence = new CharacterScannerCharSequence(scanner);

        Matcher matcher = pattern.matcher(charSequence);

        if (matcher.lookingAt()) {
            charSequence.unread(charSequence.length() - matcher.end());
            return token;
        }

        charSequence.unread();
        return Token.UNDEFINED;
	}
}

