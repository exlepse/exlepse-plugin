package org.exlepse.xle.editor.lfg.rules;

import org.eclipse.jface.text.rules.IWordDetector;

public class ConfigSectionKeyWordDetector implements IWordDetector {

	public boolean isWordPart(char c) {
		return Character.isLetter(c);
	}

	public boolean isWordStart(char c) {
		return Character.isLetter(c);
	}

}
