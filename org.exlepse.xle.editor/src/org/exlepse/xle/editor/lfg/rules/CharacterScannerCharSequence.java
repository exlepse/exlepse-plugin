package org.exlepse.xle.editor.lfg.rules;

import org.eclipse.jface.text.rules.ICharacterScanner;

public class CharacterScannerCharSequence implements CharSequence
{
    private StringBuffer buffer;

    private ICharacterScanner scanner;

    private int column;

    public CharacterScannerCharSequence(ICharacterScanner scanner)
    {
        this.scanner = scanner;

        buffer = new StringBuffer();

        column = scanner.getColumn();

        int c;
        while ((c = scanner.read()) != ICharacterScanner.EOF) {
            buffer.append((char) c);
        }
    }

    public int getColumn()
    {
        return column;
    }

    public char charAt(int index)
    {
        return buffer.charAt(index);
    }

    public int length()
    {
        return buffer.length();
    }

    public CharSequence subSequence(int start, int end)
    {
        return buffer.subSequence(start, end);
    }

    public void unread()
    {
        unread(buffer.length());
    }

    public void unread(int n)
    {
        for (int i = 0; i <= n; i++) {
            scanner.unread();
        }
    }

    @Override
    public String toString()
    {
        return buffer.toString();
    }

}