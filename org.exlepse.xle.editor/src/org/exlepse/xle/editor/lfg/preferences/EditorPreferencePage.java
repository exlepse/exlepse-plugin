package org.exlepse.xle.editor.lfg.preferences;

import org.eclipse.jface.preference.ColorFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FontFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.exlepse.xle.editor.XLEPlugin;

public class EditorPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	@Override
	protected void createFieldEditors() {
		
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		addField(new LabelFieldEditor("Common Colors", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.DEFAULT_COLOR, "Default Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.COMMENT_COLOR, "Comment Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.SECTION_HEADER_COLOR, "Section Header Color\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.SECTION_FOOTER_COLOR, "Section Footer Color\t\t", getFieldEditorParent()));
		
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		addField(new LabelFieldEditor("Config Section Colors", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.CONFIG_KEY_COLOR, "Key Color\t\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.CONFIG_SECTION_VALUE_COLOR, "Section Value Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.CONFIG_SINGLE_VALUE_COLOR, "Single Value Color\t\t", getFieldEditorParent()));		
		
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		addField(new LabelFieldEditor("Rule Section Colors", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.RULE_SOURCE_COLOR, "Rule Source Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.RULE_ARROW_COLOR, "Rule Arrow Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.RULE_TARGET_COLOR, "Rule Target Color\t\t", getFieldEditorParent()));
		
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		addField(new LabelFieldEditor("Lexicon Section Colors", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.LEXICON_WORD_COLOR, "Word Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.LEXICON_PARTOFSPEECH_COLOR, "Part of Speech Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.LEXICON_MORPHCODE_COLOR, "Morph Code Color\t\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.LEXICON_SCHEMATA_COLOR, "Schemata Color\t\t", getFieldEditorParent()));
		
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		addField(new LabelFieldEditor("Template Section Colors", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.TEMPLATE_NAME_COLOR, "Template Name Color\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.TEMPLATE_EQUALSIGN_COLOR, "Template Equal Sign Color\t", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.TEMPLATE_BODY_COLOR, "Template Body Color\t", getFieldEditorParent()));
		
		addField(new SpacerFieldEditor(getFieldEditorParent()));
		addField(new LabelFieldEditor("Morphology Section Colors", getFieldEditorParent()));
		addField(new ColorFieldEditor(XLEPreferences.MORPHOLOGY_SECTION_COLOR, "Morphology Color\t\t", getFieldEditorParent()));
		
		
		
	}

	public void init(IWorkbench workbench) {
		setPreferenceStore(XLEPlugin.getDefault().getPreferenceStore());
		setDescription("XLE Editor Settings");
	}
}
