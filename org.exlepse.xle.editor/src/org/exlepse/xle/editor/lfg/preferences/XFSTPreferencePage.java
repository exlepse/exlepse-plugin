/**
 * 
 */
package org.exlepse.xle.editor.lfg.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.exlepse.xle.editor.XLEPlugin;

/**
 * @author raedle
 *
 */
public class XFSTPreferencePage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		addField(new FileFieldEditor(XLEPreferences.XFST_PATH, "xfst Path", getFieldEditorParent()));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		setPreferenceStore(XLEPlugin.getDefault().getPreferenceStore());
		setDescription("XFST Settings");		
	}
}
