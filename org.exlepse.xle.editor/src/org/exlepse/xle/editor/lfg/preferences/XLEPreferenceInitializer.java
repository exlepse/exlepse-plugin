/**
 * 
 */
package org.exlepse.xle.editor.lfg.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;
import org.exlepse.xle.editor.XLEPlugin;

/**
 * <code>XLEPreferenceInitializer</code>.
 * 
 * <pre>
 * Date: Aug 4, 2010
 * Time: 10:50:35 AM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class XLEPreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 * 
	 */
	public XLEPreferenceInitializer() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer#initializeDefaultPreferences()
	 */
	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore preferenceStore = XLEPlugin.getDefault().getPreferenceStore();
		preferenceStore.setDefault(XLEPreferences.XLE_PATH, "/usr/local/xle");
		preferenceStore.setDefault(XLEPreferences.LD_LIBRARY_PATH, "/usr/local/xle/lib");
		preferenceStore.setDefault(XLEPreferences.DYLD_LIBRARY_PATH, "/usr/local/xle/lib:/usr/local/xle/bin/sp-3.12.7");
		preferenceStore.setDefault(XLEPreferences.DOCUMENTATION_PATH, "/usr/local/xle/doc");
		preferenceStore.setDefault(XLEPreferences.LFG_FILE_ENCODING, "utf-8");
		preferenceStore.setDefault(XLEPreferences.USE_DISPLAY, false);
		preferenceStore.setDefault(XLEPreferences.DISPLAY, ":0");
		preferenceStore.setDefault(XLEPreferences.X11_PATH, "/Applications/Utilities/X11.app");
		preferenceStore.setDefault(XLEPreferences.XFST_PATH, "/usr/local/bin");
		
		// XLE Commands
		preferenceStore.setDefault(XLEPreferences.CMD_CREATE_PARSER, "create-parser \"{0}\"");
		preferenceStore.setDefault(XLEPreferences.CMD_PARSE, "parse \"{0}\"");
		preferenceStore.setDefault(XLEPreferences.CMD_SET_CHARACTER_ENCODING, "set-character-encoding stdio {0}");
		preferenceStore.setDefault(XLEPreferences.PROBLEMS_REGEXES, "\\s*parse error at (.*)\\s*;\\s*Possible missing \\e. or quote near headword (.*) line ([\\d]+) file (.*)\\s*;\\s*Template invocation error near line ([\\d]+), column ([\\d]+)\\s*;\\s*parse error at (.*), maybe near line ([\\d]+), column ([\\d]+) in (.*)\\s*;\\s*Bad meta designator near line ([\\d]+), column ([\\d]+)\\s*");
		
		// Editor Colors
		preferenceStore.setDefault(XLEPreferences.DEFAULT_COLOR, "0,0,0");
		preferenceStore.setDefault(XLEPreferences.COMMENT_COLOR, "100,149,95");
		
		preferenceStore.setDefault(XLEPreferences.SECTION_HEADER_COLOR, "127,0,85");
		preferenceStore.setDefault(XLEPreferences.SECTION_FOOTER_COLOR, "127,0,85");
		
		preferenceStore.setDefault(XLEPreferences.CONFIG_KEY_COLOR, "127,159,191");
		preferenceStore.setDefault(XLEPreferences.CONFIG_SINGLE_VALUE_COLOR, "0,0,0");
		preferenceStore.setDefault(XLEPreferences.CONFIG_SECTION_VALUE_COLOR, "127,0,85");
		
		preferenceStore.setDefault(XLEPreferences.RULE_SOURCE_COLOR, "127,159,191");
		preferenceStore.setDefault(XLEPreferences.RULE_ARROW_COLOR, "0,0,0");
		preferenceStore.setDefault(XLEPreferences.RULE_TARGET_COLOR, "127,0,85");
		
		
		preferenceStore.setDefault(XLEPreferences.LEXICON_WORD_COLOR, "127,159,191");
		preferenceStore.setDefault(XLEPreferences.LEXICON_PARTOFSPEECH_COLOR, "0,70,15");
		preferenceStore.setDefault(XLEPreferences.LEXICON_MORPHCODE_COLOR, "0,0,0");
		preferenceStore.setDefault(XLEPreferences.LEXICON_SCHEMATA_COLOR, "163,132,140");
		
		preferenceStore.setDefault(XLEPreferences.TEMPLATE_NAME_COLOR, "127,159,191");
		preferenceStore.setDefault(XLEPreferences.TEMPLATE_EQUALSIGN_COLOR, "0,0,0");
		preferenceStore.setDefault(XLEPreferences.TEMPLATE_BODY_COLOR, "163,132,140");
		
		preferenceStore.setDefault(XLEPreferences.MORPHOLOGY_SECTION_COLOR, "0,0,0");
		
	}
}
