package org.exlepse.xle.editor.lfg.preferences;

import java.io.File;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.FileFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.exlepse.xle.editor.XLEPlugin;

public class XLEPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	private static final String[][] ENCODING_NAMES = new String[][] { { "UTF-8", "utf-8" },
			{ "ISO8859-1", "iso8859-1" }
	// {"", "cp860"},
	// {"", "cp861"},
	// {"", "cp862"},
	// {"", "cp863"},
	// {"", "tis-620"},
	// {"", "cp864"},
	// {"", "cp865"},
	// {"", "cp866"},
	// {"", "gb12345"},
	// {"", "cp949"},
	// {"", "cp950"},
	// {"", "cp869"},
	// {"", "dingbats"},
	// {"", "ksc5601"},
	// {"", "macCentEuro"},
	// {"", "cp874"},
	// {"", "macUkraine"},
	// {"", "jis0201"},
	// {"", "gb2312"},
	// {"", "euc-cn"},
	// {"", "euc-jp"},
	// {"", "macThai"},
	// {"", "iso8859-10"},
	// {"", "jis0208"},
	// {"", "iso2022-jp"},
	// {"", "macIceland"},
	// {"", "iso2022"},
	// {"", "iso8859-13"},
	// {"", "jis0212"},
	// {"", "iso8859-14"},
	// {"", "iso8859-15"},
	// {"", "X11ControlChars"},
	// {"", "cp737"},
	// {"", "iso8859-16"},
	// {"", "big5"},
	// {"", "euc-kr"},
	// {"", "macRomania"},
	// {"", "ucs-2be"},
	// {"", "macTurkish"},
	// {"", "gb1988"},
	// {"", "iso2022-kr"},
	// {"", "macGreek"},
	// {"", "ascii"},
	// {"", "cp437"},
	// {"", "macRoman"},
	// {"", "iso8859-2"},
	// {"", "iso8859-3"},
	// {"", "macCroatian"},
	// {"", "koi8-r"},
	// {"", "iso8859-4"},
	// {"", "ebcdic"},
	// {"", "iso8859-5"},
	// {"", "cp1250"},
	// {"", "macCyrillic"},
	// {"", "iso8859-6"},
	// {"", "cp1251"},
	// {"", "macDingbats"},
	// {"", "koi8-u"},
	// {"", "iso8859-7"},
	// {"", "cp1252"},
	// {"", "iso8859-8"},
	// {"", "cp1253"},
	// {"", "iso8859-9"},
	// {"", "cp1254"},
	// {"", "cp1255"},
	// {"", "cp850"},
	// {"", "cp1256"},
	// {"", "cp932"},
	// {"", "identity"},
	// {"", "cp1257"},
	// {"", "cp852"},
	// {"", "macJapan"},
	// {"", "cp1258"},
	// {"", "shiftjis"},
	// {"", "cp855"},
	// {"", "cp936"},
	// {"", "symbol"},
	// {"", "cp775"},
	// {"", "unicode"},
	// {"", "cp857"}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		setPreferenceStore(XLEPlugin.getDefault().getPreferenceStore());
		setDescription("XLE Environment Settings");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	protected void createFieldEditors() {
		// addField(new StringFieldEditor("Test", "Some Option", getFieldEditorParent()));

		addField(new DirectoryFieldEditor(XLEPreferences.XLE_PATH, "XLE Path", getFieldEditorParent()));
		addField(new StringFieldEditor(XLEPreferences.LD_LIBRARY_PATH, "LD_LIBRARY_PATH", getFieldEditorParent()));
		addField(new StringFieldEditor(XLEPreferences.DYLD_LIBRARY_PATH, "DYLD_LIBRARY_PATH", getFieldEditorParent()));
		
		addField(new DirectoryFieldEditor(XLEPreferences.DOCUMENTATION_PATH, "Documentation Path",
				getFieldEditorParent()) {

			/*
			 * (non-Javadoc)
			 * 
			 * @see org.eclipse.jface.preference.DirectoryFieldEditor#doCheckState()
			 */
			@Override
			protected boolean doCheckState() {
				boolean checkState = super.doCheckState();
				return !checkState ? false : doCheckIndexFile();
			}

			/**
			 * @return
			 */
			private boolean doCheckIndexFile() {
				String fileName = getTextControl().getText();
				fileName = fileName.trim();
				File file = new File(fileName, "xle_toc.html");
				if (!file.exists()) {
					setErrorMessage("Could not find xle_toc.html in the directory");
				}
				return file.exists();
			}
		});

		addField(new ComboFieldEditor(XLEPreferences.LFG_FILE_ENCODING, "Lfg File Encoding", ENCODING_NAMES,
				getFieldEditorParent()));
		
		addField(new FileFieldEditor(XLEPreferences.X11_PATH, "X11 Path", getFieldEditorParent()) {
			@Override
			protected boolean checkState() {
				String msg = null;

		        String path = getTextControl().getText();
		        if (path != null) {
					path = path.trim();
				} else {
					path = "";//$NON-NLS-1$
				}
		        if (path.length() == 0) {
		            if (!isEmptyStringAllowed()) {
						msg = getErrorMessage();
					}
		        } else {
		            File file = new File(path);
		            if (file.getName().endsWith(".app")) {
		                if (!file.isAbsolute()) {
							msg = JFaceResources
		                            .getString("FileFieldEditor.errorMessage2");//$NON-NLS-1$
						}
		            } else {
		                msg = getErrorMessage();
		            }
		        }

		        if (msg != null) { // error
		            showErrorMessage(msg);
		            return false;
		        }

		        // OK!
		        clearErrorMessage();
		        return true;
			}
		});
		
		final Composite fieldEditorParent = getFieldEditorParent();
		final StringFieldEditor stringFieldEditor = new StringFieldEditor(XLEPreferences.DISPLAY, "Display", fieldEditorParent);
		
		addField(new BooleanFieldEditor(XLEPreferences.USE_DISPLAY, "Use Display", getFieldEditorParent()) {
			
			/* (non-Javadoc)
			 * @see org.eclipse.jface.preference.BooleanFieldEditor#doLoad()
			 */
			@Override
			protected void doLoad() {
				super.doLoad();
				
				adjustDisplayEditor(getBooleanValue());
			}
			
			/* (non-Javadoc)
			 * @see org.eclipse.jface.preference.BooleanFieldEditor#valueChanged(boolean, boolean)
			 */
			@Override
			protected void valueChanged(boolean oldValue, boolean newValue) {
				super.valueChanged(oldValue, newValue);
				
				adjustDisplayEditor(newValue);
			}
			
			/**
			 * @param value
			 */
			private void adjustDisplayEditor(boolean value) {
				stringFieldEditor.getTextControl(fieldEditorParent).setEditable(value);
				stringFieldEditor.getTextControl(fieldEditorParent).setEnabled(value);
			}
		});
		addField(stringFieldEditor);
	}
}
