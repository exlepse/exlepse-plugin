/**
 * 
 */
package org.exlepse.xle.editor.lfg.preferences;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.ListEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.exlepse.xle.editor.XLEPlugin;

/**
 * @author raedle
 *
 */
public class ParserPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	/* (non-Javadoc)
	 * @see org.eclipse.jface.preference.FieldEditorPreferencePage#createFieldEditors()
	 */
	@Override
	protected void createFieldEditors() {
		addField(new StringFieldEditor(XLEPreferences.CMD_CREATE_PARSER, "Create Parser", getFieldEditorParent()));
		addField(new StringFieldEditor(XLEPreferences.CMD_PARSE, "Parse", getFieldEditorParent()));
		
		addField(new StringFieldEditor(XLEPreferences.CMD_SET_CHARACTER_ENCODING, "Set Character Encoding", getFieldEditorParent()));
		
		addField(new ListEditor(XLEPreferences.PROBLEMS_REGEXES, "Problem Regexes", getFieldEditorParent()) {
			
			/* (non-Javadoc)
			 * @see org.eclipse.jface.preference.ListEditor#parseString(java.lang.String)
			 */
			@Override
			protected String[] parseString(String stringList) {
				return stringList.split("\\;");
			}
			
			/* (non-Javadoc)
			 * @see org.eclipse.jface.preference.ListEditor#getNewInputObject()
			 */
			@Override
			protected String getNewInputObject() {
				InputDialog dialog = new InputDialog(getShell(), "Regex", "Problem Regex", "", null);
				int returnCode = dialog.open();
				if (returnCode != InputDialog.OK) return null;
				
				return dialog.getValue().trim();
			}
			
			/* (non-Javadoc)
			 * @see org.eclipse.jface.preference.ListEditor#createList(java.lang.String[])
			 */
			@Override
			protected String createList(String[] items) {
				StringBuilder sb = new StringBuilder();
				int len = items.length;
				for (int i = 0; i < len; i++) {
					sb.append(items[i].trim());
					if (i < len - 1) {
						sb.append(";");
					}
				}
				return sb.toString();
			}
		});
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench) {
		setPreferenceStore(XLEPlugin.getDefault().getPreferenceStore());
		setDescription("XLE Parser Settings");
	}
}
