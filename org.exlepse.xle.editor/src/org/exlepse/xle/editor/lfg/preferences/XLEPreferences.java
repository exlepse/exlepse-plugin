/**
 * 
 */
package org.exlepse.xle.editor.lfg.preferences;

import org.eclipse.core.runtime.QualifiedName;
import org.exlepse.xle.editor.XLEPlugin;

/**
 * <code>XLEPreferences</code>.
 * 
 * <pre>
 * Date: Aug 4, 2010
 * Time: 10:11:25 AM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public interface XLEPreferences {

	// Set As Main XLE
	public static final QualifiedName XLE_PROJECT_MAIN = new QualifiedName(XLEPlugin.ID, "xle_project_main");

	// Runtime

	// XLE Encoding
	public static final String LFG_FILE_ENCODING = "lfg_file_encoding";

	// Environment Settings
	public static final String XLE_PATH = "xle_path";
	public static final String LD_LIBRARY_PATH = "ld_library_path";
	public static final String DYLD_LIBRARY_PATH = "dyld_library_path";
	public static final String DOCUMENTATION_PATH = "documentation_path";
	public static final String DISPLAY = "display";
	public static final String USE_DISPLAY = "use_display";
	public static final String X11_PATH = "x11_path";
	public static final String XFST_PATH = "xfst_path";

	// XLE Commands
	public static final String CMD_CREATE_PARSER = "cmd_create_parser";
	public static final String CMD_PARSE = "cmd_parse";
	public static final String CMD_SET_CHARACTER_ENCODING = "cmd_set_character_encoding";
	public static final String PROBLEMS_REGEXES = "problems_regexes";
	
	// Editor Colors

	// Default
	public static final String DEFAULT_COLOR = "default_color";

	// Comments
	public static final String COMMENT_COLOR = "comment_color";

	// Section Header
	public static final String SECTION_HEADER_COLOR = "section_header_color";

	// Section Footer
	public static final String SECTION_FOOTER_COLOR = "section_footer_color";

	// Config Section
	public static final String CONFIG_KEY_COLOR = "config_key_color";
	public static final String CONFIG_SINGLE_VALUE_COLOR = "config_single_value_color";
	public static final String CONFIG_SECTION_VALUE_COLOR = "config_section_value_color";

	// Rule section
	public static final String RULE_SOURCE_COLOR = "rule_source_color";
	public static final String RULE_ARROW_COLOR = "rule_arrow_color";
	public static final String RULE_TARGET_COLOR = "rule_target_color";
	
	// Lexicon section
	public static final String LEXICON_WORD_COLOR = "lexicon_word_color";
	public static final String LEXICON_PARTOFSPEECH_COLOR = "lexicon_partofspeech_color";
	public static final String LEXICON_MORPHCODE_COLOR = "lexicon_morphcode_color";
	public static final String LEXICON_SCHEMATA_COLOR = "lexicon_schemata_color";

	// Template section
	public static final String TEMPLATE_NAME_COLOR = "template_name_color";
	public static final String TEMPLATE_EQUALSIGN_COLOR = "template_equalsign_color";
	public static final String TEMPLATE_BODY_COLOR = "template_body_color";
	

	// Morphology section
	public static final String MORPHOLOGY_SECTION_COLOR = "morphology_section_color";
}
