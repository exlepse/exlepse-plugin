/**
 * 
 */
package org.exlepse.xle.editor.lfg;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.text.source.Annotation;
import org.eclipse.jface.text.source.IAnnotationModel;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.IVerticalRuler;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.jface.text.source.projection.ProjectionAnnotation;
import org.eclipse.jface.text.source.projection.ProjectionAnnotationModel;
import org.eclipse.jface.text.source.projection.ProjectionSupport;
import org.eclipse.jface.text.source.projection.ProjectionViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.editors.text.TextFileDocumentProvider;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.TextOperationAction;
import org.eclipse.ui.views.contentoutline.IContentOutlinePage;
import org.exlepse.xle.AbstractItem;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.editor.common.ColorManager;
import org.exlepse.xle.editor.common.XLEProcess;
import org.exlepse.xle.editor.lfg.outline.LfgContentOutlinePage;
import org.exlepse.xle.io.IPrintable;

/**
 * <code>LfgEditor</code>.
 * 
 * <pre>
 * Date: Jul 4, 2010
 * Time: 7:08:05 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class LfgEditor extends TextEditor implements ISelectionChangedListener,
		org.eclipse.jface.util.IPropertyChangeListener {

	private final ColorManager colorManager = new ColorManager();

	private ProjectionSupport projectionSupport;
	private ProjectionAnnotationModel annotationModel;

	private LfgContentOutlinePage outlinePage;

	public LfgEditor() {
		setKeyBindingScopes(new String[] { "org.eclipse.ui.textEditorScope" });
		setSourceViewerConfiguration(new LfgSourceViewerConfiguration(colorManager, this));
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.init(site, input);
		XLEPlugin.getDefault().getPreferenceStore().addPropertyChangeListener(this);
	}

	@Override
	protected ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {

		ISourceViewer viewer = new ProjectionViewer(parent, ruler, getOverviewRuler(), isOverviewRulerVisible(), styles);

		// ensure decoration support has been created and configured.
		getSourceViewerDecorationSupport(viewer);

		return viewer;

	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		ProjectionViewer viewer = (ProjectionViewer) getSourceViewer();

		projectionSupport = new ProjectionSupport(viewer, getAnnotationAccess(), getSharedColors());
		projectionSupport.install();

		// turn projection mode on
		viewer.doOperation(ProjectionViewer.TOGGLE);

		annotationModel = viewer.getProjectionAnnotationModel();
	}

	private Annotation[] oldAnnotations;

	public void updateFoldingStructure(ArrayList positions) {
		Annotation[] annotations = new Annotation[positions.size()];

		// this will hold the new annotations along
		// with their corresponding positions
		HashMap newAnnotations = new HashMap();

		for (int i = 0; i < positions.size(); i++) {
			ProjectionAnnotation annotation = new ProjectionAnnotation();

			newAnnotations.put(annotation, positions.get(i));

			annotations[i] = annotation;
		}

		annotationModel.modifyAnnotations(oldAnnotations, newAnnotations, null);

		oldAnnotations = annotations;
	}

	@Override
	public void dispose() {

		XLEPlugin.getDefault().getPreferenceStore().removePropertyChangeListener(this);

		super.dispose();
	}

	private IDocumentProvider createDocumentProvider(IEditorInput input) {
		if (input instanceof IFileEditorInput) {
			return new LfgDocumentProvider();
		}
		return new TextFileDocumentProvider();
	}

	@Override
	protected void doSetInput(IEditorInput input) throws CoreException {
		setDocumentProvider(createDocumentProvider(input));
		super.doSetInput(input);
	}

	@Override
	public void doSave(IProgressMonitor progressMonitor) {
		super.doSave(progressMonitor);

		if (outlinePage != null) {
			outlinePage.setInput(getEditorInput());
		}

		IEditorInput editorInput = getEditorInput();
		if (editorInput instanceof FileEditorInput) {
			IFile file = ((FileEditorInput) editorInput).getFile();
			IProject project = file.getProject();
			try {
				XLEProcess.update(project);
			}
			catch (CoreException e) {
				try {
					IMarker marker = project.createMarker(IMarker.PROBLEM);
					marker.setAttribute(IMarker.MESSAGE, e.getMessage());
					marker.setAttribute(IMarker.TRANSIENT, true);
					marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
				}
				catch (CoreException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.editors.text.TextEditor#getAdapter(java.lang.Class)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object getAdapter(Class adapter) {

		if (IContentOutlinePage.class.equals(adapter)) {
			if (outlinePage == null) {
				outlinePage = new LfgContentOutlinePage(getDocumentProvider(), this);
				outlinePage.addSelectionChangedListener(this);
			}
			outlinePage.setInput(getEditorInput());
			return outlinePage;
		}
		return super.getAdapter(adapter);
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createSourceViewer(org.eclipse.swt.widgets.Composite,
	// * org.eclipse.jface.text.source.IVerticalRuler, int)
	// */
	// @Override
	// protected ISourceViewer createSourceViewer(Composite parent, IVerticalRuler ruler, int styles) {
	// return new ProjectionViewer(parent, ruler, fOverviewRuler, true, styles);
	// }
	//
	// /*
	// * (non-Javadoc)
	// *
	// * @see org.eclipse.ui.texteditor.AbstractDecoratedTextEditor#createPartControl(org.eclipse.swt.widgets.Composite)
	// */
	// @Override
	// public void createPartControl(Composite parent) {
	// super.createPartControl(parent);
	//
	// ProjectionViewer projectionViewer = (ProjectionViewer) getSourceViewer();
	// fProjectionSupport = new ProjectionSupport(projectionViewer, getAnnotationAccess(), getSharedColors());
	// fProjectionSupport.install();
	// projectionViewer.doOperation(ProjectionViewer.TOGGLE);
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.viewers.ISelectionChangedListener#selectionChanged(org.eclipse.jface.viewers.SelectionChangedEvent
	 * )
	 */
	public void selectionChanged(SelectionChangedEvent event) {
		ISelection selection = event.getSelection();
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			Object o = structuredSelection.getFirstElement();

			if (o instanceof AbstractItem) {
				// IPrintable printable = (IPrintable) o;
				// StringDevice device = new StringDevice();
				// try {
				// printable.print(device);
				// }
				// catch (IOException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				if (o != null) {
					String word = ((AbstractItem) o).getRaw();
					// String word = device.toString();

					IAnnotationModel model = getSourceViewer().getAnnotationModel();
					// Remove old annotations.
					for (Iterator<Annotation> iterator = model.getAnnotationIterator(); iterator.hasNext();) {
						Annotation annotation = iterator.next();
						model.removeAnnotation(annotation);
					}
					IDocument document = getSourceViewer().getDocument();
					createNewAnnotations(word, document, model);
				}
			}
		}
	}

	private void createNewAnnotations(String word, IDocument document, IAnnotationModel model) {
		String content = document.get();
		int idx = content.indexOf(word);
		int lenIdx;
		while (idx != -1) {
			lenIdx = content.indexOf('.', idx + 1);

			Annotation annotation = new Annotation("org.exlepse.xle.editor.lfg.textMarker", false, word);
			Position position = new Position(idx, lenIdx - idx + 1);

			model.addAnnotation(annotation, position);
			updateMarkerViews(annotation);
			// fOldAnnotations.add(annotation);

			idx = content.indexOf(word, lenIdx + 1);
		}
	}

	public void propertyChange(org.eclipse.jface.util.PropertyChangeEvent event) {

		String property = event.getProperty();

		// System.out.println(property);

		if (property.contains("color")) {
			SourceViewer sw = (SourceViewer) getSourceViewer();
			sw.refresh();
			// System.out.println(sw.toString());
		}

	}

	@Override
	protected void createActions() {

		super.createActions();

		IAction a = new TextOperationAction(LfgEditorMessages.getResourceBundle(),
				"ContentFormatProposal.", this, ISourceViewer.FORMAT); //$NON-NLS-1$
		setAction("ContentFormatProposal", a);

	}
}
