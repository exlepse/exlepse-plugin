package org.exlepse.xle.editor.lfg.formatter;

import java.util.ArrayList;
import java.util.Scanner;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.ConfigSection;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SectionFeature;

public class ConfigSectionFormattingStrategy extends DefaultFormattingStrategy {

	private final int maxKeyLength;

	public ConfigSectionFormattingStrategy() {
		int maxLength = 0;
		for (String feature : ConfigSection.ConfigFeatures) {
			maxLength = Math.max(maxLength, feature.length());
		}
		maxKeyLength = maxLength;
	}

	@Override
	public String format(String content, boolean isLineStart, String indentation, int[] positions) {
		content = content.trim();

		int headerEnd = content.indexOf('\n');
		int footerOffset = content.lastIndexOf("----");

		String header = content.substring(0, headerEnd);
		String footer = content.substring(footerOffset, footerOffset + 4);

		content = content.substring(headerEnd, footerOffset).trim();

		StringBuffer sb = new StringBuffer();

		sb.append(header);
		sb.append(lineSeparator);
		sb.append(lineSeparator);

		Scanner scanner = new Scanner(content + " ");
		scanner.useDelimiter("\\.\\s");

		ArrayList<IFeature> features = new ArrayList<IFeature>();

		while (scanner.hasNext()) {
			try {
				String rawFeature = scanner.next().trim();
				if (rawFeature.length() <= 0) continue;
				IFeature feature = ConfigParser.parseFeature(rawFeature);
				features.add(feature);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		scanner.close();

		for (IFeature feature : features) {
			String featureName = feature.getName();

			int lengthDiff = maxKeyLength - featureName.length();

			sb.append("\t");
			sb.append(featureName);

			for (int i = 0; i < lengthDiff; i++)
				sb.append(" ");
			sb.append("\t");

			for (String value : feature.getValues()) {
				if (feature instanceof SectionFeature) sb.append('(');

				sb.append(value);

				if (feature instanceof SectionFeature) sb.append(')');

				sb.append(" ");
			}

			String soFar = sb.toString();

			sb = new StringBuffer(soFar.substring(0, soFar.length() - 1));

			sb.append('.');

			sb.append(lineSeparator);
		}

		sb.append(lineSeparator);
		sb.append(footer);
		sb.append(lineSeparator);
		sb.append(lineSeparator);

		return sb.toString();
	}

}
