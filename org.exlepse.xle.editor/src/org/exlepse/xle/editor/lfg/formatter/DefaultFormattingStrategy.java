package org.exlepse.xle.editor.lfg.formatter;

import org.eclipse.jface.text.formatter.IFormattingStrategy;

public class DefaultFormattingStrategy implements IFormattingStrategy {
	
	protected static final String lineSeparator = System.getProperty("line.separator");

	public DefaultFormattingStrategy()
	{
		super();
	}

	public String format(String content, boolean isLineStart,String indentation, int[] positions) {
		return content;
	}

	public void formatterStarts(String initialIndentation)
	{
	}

	public void formatterStops()
	{
	}

}
