/**
 * 
 */
package org.exlepse.xle.editor.lfg.outline;

import java.io.IOException;

import org.eclipse.jface.viewers.AbstractTreeViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.views.contentoutline.ContentOutlinePage;
import org.exlepse.xle.DocumentParser;
import org.exlepse.xle.XLEDocument;
import org.exlepse.xle.config.ConfigSection;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.editor.XLEPlugin;
import org.exlepse.xle.lexicon.LexiconEntry;
import org.exlepse.xle.lexicon.LexiconSection;
import org.exlepse.xle.rules.Rule;
import org.exlepse.xle.rules.RulesSection;
import org.exlepse.xle.section.ISection;
import org.exlepse.xle.templates.Template;
import org.exlepse.xle.templates.TemplatesSection;

/**
 * <code>LfgContentOutlinePage</code>.
 * 
 * <pre>
 * Date: Jul 4, 2010
 * Time: 7:30:15 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id$
 * @since 1.0.0
 */
public class LfgContentOutlinePage extends ContentOutlinePage implements ITreeContentProvider, ILabelProvider {

	private DocumentParser documentParser = new DocumentParser();
	private XLEDocument document;
	
	private IDocumentProvider documentProvider;
	private IEditorPart editorPart;
	private IEditorInput editorInput;
	
	public LfgContentOutlinePage(IDocumentProvider documentProvider, IEditorPart editorPart) {
		this.documentProvider = documentProvider;
		this.editorPart = editorPart;
	}
	
	/**
	 * @param editorInput
	 */
	public void setInput(IEditorInput input) {
		this.editorInput = input;
		
		setContent(input);
	}
	
	private void setContent(IEditorInput input) {
		if (input instanceof FileEditorInput) {
			FileEditorInput fileEditorInput = (FileEditorInput) input;
			
			try {
				document = documentParser.parse(fileEditorInput.getPath().toFile());
				
				if (getTreeViewer() != null) {
					getTreeViewer().setInput(document);
				}
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.views.contentoutline.ContentOutlinePage#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createControl(Composite parent) {
		super.createControl(parent);

		TreeViewer viewer = getTreeViewer();
		viewer.setContentProvider(this);
		viewer.setLabelProvider(this);
		viewer.setAutoExpandLevel(AbstractTreeViewer.ALL_LEVELS);
		setContent(editorPart.getEditorInput());
	}

	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof ISection) {
			ISection section = (ISection) parentElement;
			
			if (section instanceof ConfigSection) {
				ConfigSection configSection = (ConfigSection) section;
				return configSection.getFeatures().toArray(new IFeature[0]);
			}
			else if (section instanceof RulesSection) {
				RulesSection rulesSection = (RulesSection) section;
				return rulesSection.getRules().toArray(new Rule[0]);
			}
			else if (section instanceof TemplatesSection) {
				TemplatesSection templatesSection = (TemplatesSection) section;
				return templatesSection.getTemplates().toArray(new Template[0]);
			}
			else if (section instanceof LexiconSection) {
				LexiconSection lexiconSection = (LexiconSection) section;
				return lexiconSection.getLexiconEntries().toArray(new LexiconEntry[0]);
			}
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#getParent(java.lang.Object)
	 */
	public Object getParent(Object element) {
		if (!(element instanceof XLEDocument)) {
			return document;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ITreeContentProvider#hasChildren(java.lang.Object)
	 */
	public boolean hasChildren(Object element) {
		if (element instanceof XLEDocument || element instanceof ISection) {
			return true;
		}
		return false;
	}

	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof XLEDocument) {
			return ((XLEDocument) inputElement).getSections().toArray(new ISection[0]);
		}
		return null;
	}

	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
	 */
	public Image getImage(Object element) {
		if(element instanceof ConfigSection)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_square_grey.png").createImage();
		else if(element instanceof RulesSection)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_square_red.png").createImage();
		else if(element instanceof TemplatesSection)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_square_blue.png").createImage();
		else if(element instanceof LexiconSection)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_square_yellow.png").createImage();
		else if(element instanceof IFeature)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_ball_grey.png").createImage();
		else if(element instanceof Rule)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_ball_red.png").createImage();
		else if(element instanceof Template)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_ball_blue.png").createImage();
		else if(element instanceof LexiconEntry)
			return XLEPlugin.getImageDescriptor("/icons/full/obj16/bullet_ball_yellow.png").createImage();
		
		return XLEPlugin.getImageDescriptor("/icons/question.gif").createImage();
	}

	public String getText(Object element) {
//		ISection section = (ISection)element;
		return element.toString();
	}

	public void addListener(ILabelProviderListener listener) {
		
	}

	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	public void removeListener(ILabelProviderListener listener) {
		// TODO Auto-generated method stub
		
	}
}
