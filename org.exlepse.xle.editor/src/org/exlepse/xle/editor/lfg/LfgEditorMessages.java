package org.exlepse.xle.editor.lfg;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class LfgEditorMessages {

	
	private static final String RESOURCE_BUNDLE = "org.exlepse.xle.editor.lfg.LfgEditorMessages";
		
	private static ResourceBundle fgResourceBundle = ResourceBundle.getBundle(RESOURCE_BUNDLE);

	private LfgEditorMessages() {
	}
	
	public static String getString(String key)
	{
		try
		{
			return fgResourceBundle.getString(key);
		} 
		catch (MissingResourceException e)
		{
			return "!" + key + "!";
		}
	}
	
	public static ResourceBundle getResourceBundle()
	{
		return fgResourceBundle;
	}
}
