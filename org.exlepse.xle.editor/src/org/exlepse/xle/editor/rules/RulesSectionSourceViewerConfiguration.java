/**
 * 
 */
package org.exlepse.xle.editor.rules;

import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.TextAttribute;
import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.presentation.IPresentationReconciler;
import org.eclipse.jface.text.presentation.PresentationReconciler;
import org.eclipse.jface.text.rules.DefaultDamagerRepairer;
import org.eclipse.jface.text.rules.IRule;
import org.eclipse.jface.text.rules.IToken;
import org.eclipse.jface.text.rules.IWhitespaceDetector;
import org.eclipse.jface.text.rules.IWordDetector;
import org.eclipse.jface.text.rules.RuleBasedScanner;
import org.eclipse.jface.text.rules.SingleLineRule;
import org.eclipse.jface.text.rules.Token;
import org.eclipse.jface.text.rules.WhitespaceRule;
import org.eclipse.jface.text.rules.WordRule;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.RGB;
import org.exlepse.xle.editor.common.ColorManager;

/**
 * <code>RuleConfiguration</code>.
 * 
 * <pre>
 * Date: Jul 1, 2010
 * Time: 9:19:11 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: RulesSectionSourceViewerConfiguration.java 8100 2010-07-02 18:18:19Z raedle $
 * @since 1.0.0
 */
public class RulesSectionSourceViewerConfiguration extends SourceViewerConfiguration {

	private PresentationReconciler reconciler;

	private ColorManager colorManager = new ColorManager();

	public RulesSectionSourceViewerConfiguration(ColorManager colorManager) {
		this.colorManager = colorManager;
	}

	public IPresentationReconciler getPresentationReconciler(ISourceViewer sourceViewer) {
		if (reconciler != null) return reconciler;

		reconciler = new PresentationReconciler();

		// 1) Damager and repairer for RulesSection.
		RuleBasedScanner scanner = new RuleBasedScanner();

		IToken defaultToken = new Token(new TextAttribute(colorManager.getColor(new RGB(0, 0, 0)), null, SWT.BOLD));
		IToken stringToken = new Token(new TextAttribute(colorManager.getColor(new RGB(0, 128, 0)), null, SWT.BOLD | SWT.ITALIC));

		IRule[] rules = new IRule[2];

		// The white space rule.
		IWhitespaceDetector whitespaceDetector = new IWhitespaceDetector() {
			public boolean isWhitespace(char c) {
				return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
			}
		};

		// the rule for double quotes
		rules[0] = new SingleLineRule("\"", "\"", stringToken, '\\');
		rules[1] = new WhitespaceRule(whitespaceDetector);

		scanner.setRules(rules);
		scanner.setDefaultReturnToken(defaultToken);

		DefaultDamagerRepairer dr = new DefaultDamagerRepairer(scanner);

		reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		// // 2) Damager and repairer for JNLP default content type.
		// IToken procInstr = new Token(new TextAttribute(colorManager.getColor(new RGB(255, 255, 255))));
		//
		// rules = new IRule[2];
		// // the rule for processing instructions
		// rules[0] = new SingleLineRule("<?", "?>", procInstr);
		// // the rule for generic whitespace.
		// rules[1] = new WhitespaceRule(whitespaceDetector);
		//
		// scanner = new RuleBasedScanner();
		// scanner.setRules(rules);
		// scanner.setDefaultReturnToken(new Token(new TextAttribute(colorManager.getColor(new RGB(255, 255, 30)))));
		//
		// dr = new DefaultDamagerRepairer(scanner);
		// reconciler.setDamager(dr, IDocument.DEFAULT_CONTENT_TYPE);
		// reconciler.setRepairer(dr, IDocument.DEFAULT_CONTENT_TYPE);

		return reconciler;
	}

//	/* (non-Javadoc)
//	 * @seeSourceViewerConfiguration#getContentAssistant(ISourceViewer)
//	 */
//	public IContentAssistant getContentAssistant(ISourceViewer sourceViewer) {
//		ContentAssistant assistant = new ContentAssistant();
//
//		IContentAssistProcessor processor = new RuleCAProcessor();
//
//		assistant.setContentAssistProcessor(processor, "FF0000");
//
//		assistant.setContentAssistProcessor(processor, IDocument.DEFAULT_CONTENT_TYPE);
//
//		assistant.enableAutoActivation(true);
//		assistant.setAutoActivationDelay(500); // 0.5 s.
//
//		return assistant;
//	}
}
