/**
 * 
 */
package org.exlepse.xle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.lexicon.LexiconParser;
import org.exlepse.xle.rules.RulesParser;
import org.exlepse.xle.section.RawSectionParser;
import org.exlepse.xle.section.SectionHeader;
import org.exlepse.xle.section.SectionHeaderParser;
import org.exlepse.xle.templates.TemplatesParser;

/**
 * <code>DocumentParser</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:56:33 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: DocumentParser.java 9159 2010-08-10 10:02:56Z raedle $
 * @since 1.0.0
 */
public class DocumentParser implements IParser<XLEDocument> {

	public XLEDocument parse(File file) throws IOException {
		return parse(readFileAsString(file.getAbsolutePath()));
	}
	
	public XLEDocument parse(String str) {
		XLEDocument document = new XLEDocument();

		Scanner scanner = new Scanner(str);
		scanner.useDelimiter("----");

		while (scanner.hasNext()) {
			
			try
			{
			String content = scanner.next().trim();
			if (content.length() <= 0) {
				continue;
			}

			int indexOfLineBreak = content.indexOf("\n");
			
			boolean hasContent = indexOfLineBreak > 0 ? true : false;

			String header = null;
			
			if(hasContent)
				header = content.substring(0, indexOfLineBreak);
			else
				header = content;

			SectionHeaderParser shp = new SectionHeaderParser();
			SectionHeader sectionHeader = shp.parse(header);

			switch (sectionHeader.getSectionType()) {
			case CONFIG:
				//Little hack: add whitespace at the end, otherwise the last feature contains the trailing point
				document.addSection(new ConfigParser(sectionHeader).parse(content + " "));
				break;
			case LEXICON:
				document.addSection(new LexiconParser(sectionHeader).parse(content));
				break;
			case RULES:
				document.addSection(new RulesParser(sectionHeader).parse(content));
				break;
			case TEMPLATES:
				document.addSection(new TemplatesParser(sectionHeader).parse(content));
				break;
			case FEATURES:
				document.addSection(new RawSectionParser(sectionHeader).parse(content));
				break;
			case MORPHOLOGY:
				document.addSection(new RawSectionParser(sectionHeader).parse(content));
				break;
			default:
				document.addSection(new RawSectionParser(sectionHeader).parse(content));
				break;
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		scanner.close();

		return document;
	}
	
	private static String readFileAsString(String filePath) throws IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}
}
