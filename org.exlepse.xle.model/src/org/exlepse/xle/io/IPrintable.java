/**
 * 
 */
package org.exlepse.xle.io;

import java.io.IOException;

/**
 * <code>IPrintable</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:51:07 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: IPrintable.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public interface IPrintable {
	void print(IDevice device) throws IOException;
}
