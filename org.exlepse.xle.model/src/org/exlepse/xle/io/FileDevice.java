/**
 * 
 */
package org.exlepse.xle.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * <code>FileDevice</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:50:53 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: FileDevice.java 9160 2010-08-10 12:15:26Z raedle $
 * @since 1.0.0
 */
public class FileDevice extends FileWriter implements IDevice {

	/**
	 * @param file
	 * @throws IOException
	 */
	public FileDevice(File file) throws IOException {
		super(file);
	}

	/**
	 * @param fileName
	 * @throws IOException
	 */
	public FileDevice(String fileName) throws IOException {
		super(fileName);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IDevice#writeWhitespace()
	 */
	public void writeWhitespace() throws IOException {
		super.write(' ');
	}
}
