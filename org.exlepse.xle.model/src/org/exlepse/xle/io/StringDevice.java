/**
 * 
 */
package org.exlepse.xle.io;

import java.io.IOException;
import java.io.StringWriter;

/**
 * <code>StringDevice</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:51:14 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: StringDevice.java 9160 2010-08-10 12:15:26Z raedle $
 * @since 1.0.0
 */
public class StringDevice implements IDevice {

	private StringWriter writer;

	public StringDevice() {
		writer = new StringWriter();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IDevice#write(char[])
	 */
	public void write(char[] cbuf) throws IOException {
		writer.write(cbuf);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IDevice#write(char[], int, int)
	 */
	public void write(char[] cbuf, int off, int len) throws IOException {
		writer.write(cbuf, off, len);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IDevice#write(int)
	 */
	public void write(int c) throws IOException {
		writer.write(c);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IDevice#write(java.lang.String)
	 */
	public void write(String str) throws IOException {
		writer.write(str);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IDevice#writeWhitespace()
	 */
	public void writeWhitespace() {
		writer.write(' ');
	}

	/**
	 * @return
	 */
	public StringBuffer getBuffer() {
		return writer.getBuffer();
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return writer.toString();
	}
}
