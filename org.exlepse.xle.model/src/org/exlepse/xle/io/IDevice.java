/**
 * 
 */
package org.exlepse.xle.io;

import java.io.IOException;

/**
 * <code>IDevice</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:51:00 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: IDevice.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public interface IDevice {
	void write(char[] buf) throws IOException;

	void write(char[] buf, int off, int len) throws IOException;

	void write(int c) throws IOException;

	void write(String str) throws IOException;

	void writeWhitespace() throws IOException;
}
