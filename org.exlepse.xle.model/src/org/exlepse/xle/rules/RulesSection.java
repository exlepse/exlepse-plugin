package org.exlepse.xle.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.section.BaseSection;
import org.exlepse.xle.section.SectionHeader;

/**
 * <code>RulesSection</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:50:22 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: RulesSection.java 8209 2010-07-05 16:49:25Z raedle $
 * @since 1.0.0
 */
public class RulesSection extends BaseSection {

	private Collection<Rule> rules;

	public RulesSection(SectionHeader header) {
		super(header);
		rules = new ArrayList<Rule>();
	}
	
	/**
	 * @return the rules
	 */
	public final Collection<Rule> getRules() {
		return rules;
	}

	public void addRule(Rule rule) {
		rules.add(rule);
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.section.BaseSection#printSectionContent(org.exlepse.xle.io.IDevice)
	 */
	@Override
	public void printSectionContent(IDevice device) throws IOException {
		for (Rule rule : rules) {
			rule.print(device);
			device.write("\n\n");
		}
	}
}
