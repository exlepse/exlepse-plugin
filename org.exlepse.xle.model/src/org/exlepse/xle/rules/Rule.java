/**
 * 
 */
package org.exlepse.xle.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.util.Stack;

import org.exlepse.xle.AbstractItem;
import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.io.IPrintable;

/**
 * <code>Rule</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:50:02 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: Rule.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class Rule extends AbstractItem implements ICategory, IPrintable {

	public static Rule create(String str) {
		Scanner scanner = new Scanner(str);
		scanner.useDelimiter("-->");

		String category = scanner.next().trim();
		String categories = null;
		try {
			categories = scanner.next().trim();
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			// System.err.println(e.getMessage() + ": " + str);
			return new Rule(str, category.substring(0, category.indexOf('(') != -1 ? category.indexOf('(') : category
					.length()));
		}

		scanner.close();

		Rule rule = new Rule(str, category);
		rule.parse(categories);

		return rule;
	}

	private final String category;

	private final Collection<ICategory> categories;

	public Rule(String raw, String category) {
		super(raw);
		this.category = category;
		categories = new ArrayList<ICategory>();
	}

	public ICategory parse(String str) {
		ICategory category = null;
		Stack<Character> bracketCheck = new Stack<Character>();

		// Stack<Stack<Character>> contentStack = new Stack<Stack<Character>>();

		boolean hasSchemata = false;
		StringBuilder content = null;
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);

			switch (c) {
			case '{':
				bracketCheck.push(c);
				category = new Disjunction();
				break;
			case '}':
				// Check whether the amount of opening curly brackets does match
				// the amount of closing curly brackets.
				if (bracketCheck.peek() != '{') {
					throw new RuntimeException(
							"A closing curly bracket '}' was found but the opening curly bracket is missing.");
				}
				bracketCheck.pop();

				// If bracket check stack is empty it will be added to the
				// categories collection.
				if (bracketCheck.isEmpty()) {
					category.parse(content.toString().trim());
					categories.add(category);
					category = null;
					content.setLength(0);
				}
				break;
			case ';':
				hasSchemata = false;
				category = new Conjunction();
				category.parse(content.toString().trim());
				categories.add(category);
				category = null;
				content.setLength(0);
				break;
			case '(':
				bracketCheck.push(c);
				if (hasSchemata) content.append(c);
				else category = new Conjunction(true);
				break;
			case ')':

				// Check whether the amount of opening brackets does match the
				// amount of closing brackets.
				if (bracketCheck.peek() != '(') {
					throw new RuntimeException("A closing bracket ')' was found but the opening bracket is missing.");
				}
				bracketCheck.pop();

				if (hasSchemata) {
					hasSchemata = false;
					content.append(c);
					break;
				}

				// If bracket check stack is empty it will be added to the
				// categories collection.
				if (bracketCheck.isEmpty()) {
					category.parse(content.toString().trim());
					categories.add(category);
					category = null;
					content.setLength(0);
				}
				break;
			case ':':
				hasSchemata = true;
				content.append(c);
				break;
			case '\n':
			case '\r':
			case '\t':
				// Ignore new line, cariage return, and tabulator
				break;
			default:
				if (content == null) {
					content = new StringBuilder();
				}
				content.append(c);

				// Create conjunction if rule is at its end and ';' therefore
				// does not exist.
				if (category == null) {
					category = new Conjunction();
				}

				break;
			}
		}

		if (content.length() > 0) {
			category.parse(content.toString().trim());
			categories.add(category);
			category = null;
			content.setLength(0);
		}

		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 */
	public void print(IDevice device) throws IOException {
		device.write(category);
		device.write(" --> ");

		int size = categories.size();
		int i = size;
		for (ICategory cat : categories) {

			if (i < size) device.write('\t');

			cat.print(device);

			if (--i > 0) {
				if (cat instanceof Conjunction && !((Conjunction) cat).isOptional()) {
					device.write(';');
				}
				device.write('\n');
			}
		}

		device.write('.');
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return category;
	}
}
