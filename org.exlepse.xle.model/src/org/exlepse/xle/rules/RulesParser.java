/**
 * 
 */
package org.exlepse.xle.rules;

import java.util.Scanner;

import org.exlepse.xle.IParser;
import org.exlepse.xle.section.SectionHeader;
import org.exlepse.xle.section.SectionParser;

/**
 * <code>RulesParser</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:50:14 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: RulesParser.java 9159 2010-08-10 10:02:56Z raedle $
 * @since 1.0.0
 */
public class RulesParser extends SectionParser implements IParser<RulesSection> {

	public RulesParser(SectionHeader sectionHeader) {
		super(sectionHeader);
	}

	/**
	 * @param content
	 * @return
	 */
	public RulesSection parse(String content) {

		RulesSection section = new RulesSection(header);

		Scanner scanner = new Scanner(content);
		scanner.useDelimiter("\\.");

		// Ignore TOY ENGLISH RULES (1.0)
		scanner.nextLine();

		while (scanner.hasNext()) {
			String rule = scanner.next().trim();

			if (rule.length() > 0) section.addRule(Rule.create(rule));
		}

		return section;
	}
}
