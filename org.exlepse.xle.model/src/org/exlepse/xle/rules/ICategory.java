/**
 * 
 */
package org.exlepse.xle.rules;

import org.exlepse.xle.IParser;
import org.exlepse.xle.io.IPrintable;

/**
 * <code>ICategory</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:49:54 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: ICategory.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public interface ICategory extends IParser<ICategory>, IPrintable {

}
