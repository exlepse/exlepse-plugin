/**
 * 
 */
package org.exlepse.xle.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.exlepse.xle.io.IDevice;

/**
 * <code>Conjunction</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:49:21 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: Conjunction.java 9160 2010-08-10 12:15:26Z raedle $
 * @since 1.0.0
 */
public class Conjunction implements ICategory {

	private Collection<ICategory> sequence;

	private boolean optional;

	public boolean isOptional() {
		return optional;
	}

	private String category;

	private String schemata;

	public Conjunction() {
		this(false);
	}

	public Conjunction(boolean optional) {
		sequence = new ArrayList<ICategory>();
		this.optional = optional;

		schemata = "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.IParser#parse(java.lang.String)
	 */
	public ICategory parse(String str) {
		String[] values = str.split(":");

		category = values[0];
		if (values.length > 1) schemata = values[1];

		//System.out.println("Conjunction: " + category + ": " + schemata);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 * 
	 * EXAMPLE VP: ^ = !; (PERIOD) "optional"
	 */
	public void print(IDevice device) throws IOException {
		if (optional) device.write('(');

		device.write(category);

		if (schemata.length() > 0) {
			device.write(": ");
			device.write(schemata);
		}

		for (ICategory cat : sequence) {
			cat.print(device);
			if (!optional) device.write(';');
		}
		if (optional) device.write(')');
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Conjunction [category=" + category + ",schemata:" + schemata + ",optional=" + optional + "]";
	}
}
