/**
 * 
 */
package org.exlepse.xle.rules;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.exlepse.xle.io.IDevice;

/**
 * <code>Disjunction</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:49:49 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: Disjunction.java 9160 2010-08-10 12:15:26Z raedle $
 * @since 1.0.0
 */
public class Disjunction implements ICategory {

	private final Collection<ICategory> alternatives;

	public Disjunction() {
		alternatives = new ArrayList<ICategory>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.IParser#parse(java.lang.String)
	 */
	public ICategory parse(String str) {
		// throw new UnsupportedOperationException("Not yet implemented.");
		// System.out.println("Not yet implemented: " + str);
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 */
	public void print(IDevice device) throws IOException {
		for (ICategory cat : alternatives) {
			device.write('{');
			cat.print(device);
			device.write('}');
		}
	}
}
