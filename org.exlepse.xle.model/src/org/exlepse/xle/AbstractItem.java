/**
 * 
 */
package org.exlepse.xle;

/**
 * @author raedle
 *
 */
public abstract class AbstractItem {
	
	private String raw;
	
	public AbstractItem(String raw) {
		this.raw = raw;
	}
	
	public String getRaw() {
		return raw;
	}
}
