/**
 * 
 */
package org.exlepse.xle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.exlepse.xle.io.StringDevice;

/**
 * <code>Main</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:56:44 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: Main.java 8209 2010-07-05 16:49:25Z raedle $
 * @since 1.0.0
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		DocumentParser xleDocumentParser = new DocumentParser();

		XLEDocument doc = xleDocumentParser.parse(readFileAsString("toy-eng.lfg"));

		StringDevice device = new StringDevice();
		doc.print(device);

		System.out.println(device);
		final File file = File.createTempFile("xle-grammar", ".lfg");
		FileOutputStream out = new FileOutputStream(file);
		out.write(device.toString().getBytes());
		out.flush();
		out.close();

		ProcessBuilder pb = new ProcessBuilder("/usr/local/XLE/bin/xle");
		pb.directory(new File("/usr/local/XLE"));
		final Process process = pb.start();
		// final Process process =
		// Runtime.getRuntime().exec("/usr/local/xle/bin/xle");
		// final Process process = Runtime.getRuntime().exec("bash", null, new
		// File("/usr/bin"));

		new Thread() {

			@Override
			public void run() {
				BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
				char[] buf = new char[8096];
				int len;
				try {
					while ((len = br.read(buf)) != -1) {
						System.err.println(String.valueOf(buf, 0, len));
					}
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();

		new Thread() {

			@Override
			public void run() {
				BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
				char[] buf = new char[8096];
				int len;
				try {
					while ((len = br.read(buf)) != -1) {
						System.out.println(String.valueOf(buf, 0, len));
					}
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();

		new Thread() {

			@Override
			public void run() {
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

				String line = null;
				try {
					while ((line = br.readLine()) != null) {
						// if ("exec".equals(line)) {
						// process.getOutputStream().write("help".getBytes());
						// process.getOutputStream().flush();
						// }
						if (line.startsWith("create-parser")) {
							process.getOutputStream().write(line.getBytes());
							process.getOutputStream().write(" ".getBytes());
							process.getOutputStream().write(file.getAbsolutePath().getBytes());
							process.getOutputStream().write("\n".getBytes());
							process.getOutputStream().flush();
						}
						else {
							process.getOutputStream().write(line.getBytes());
							process.getOutputStream().write("\n".getBytes());
							process.getOutputStream().flush();
						}
					}
				}
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}

	private static String readFileAsString(String filePath) throws IOException {
		StringBuffer fileData = new StringBuffer(1000);
		BufferedReader reader = new BufferedReader(new FileReader(filePath));
		char[] buf = new char[1024];
		int numRead = 0;
		while ((numRead = reader.read(buf)) != -1) {
			String readData = String.valueOf(buf, 0, numRead);
			fileData.append(readData);
			buf = new char[1024];
		}
		reader.close();
		return fileData.toString();
	}
}
