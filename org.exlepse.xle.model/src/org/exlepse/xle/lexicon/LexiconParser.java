/**
 * 
 */
package org.exlepse.xle.lexicon;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.exlepse.xle.IParser;
import org.exlepse.xle.section.SectionHeader;
import org.exlepse.xle.section.SectionParser;

/**
 * <code>LexiconParser</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:50:39 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: LexiconParser.java 8229 2010-07-05 21:22:40Z zoellner $
 * @since 1.0.0
 */
public class LexiconParser extends SectionParser implements IParser<LexiconSection> {

	public LexiconParser(SectionHeader sectionHeader) {
		super(sectionHeader);
	}

	/**
	 * @param content
	 * @return
	 */
	public LexiconSection parse(String content) {

		LexiconSection section = new LexiconSection(header);
		
		content = content.substring(content.indexOf('\n'));
		
		//this pattern searches for dots followed by one or more line terminators or an end of string
		Pattern pattern = Pattern.compile("([\\.])([\\n\\r]+|$)", Pattern.DOTALL);
		Matcher matcher = pattern.matcher(content);
		
		//we then go sequentially through the string and filter out all substrings starting
		//from the position after a dot to the position of the next dot
		
		int start = 0;
		int end = 0;
		
		ArrayList<String> rawEntries = new ArrayList<String>();
		
		while(matcher.find())
		{
			end = matcher.end();
			rawEntries.add(content.substring(start, end).trim());
			start = end;
		}
		
		//those substrings are then feeded into the LexiconEntry parser which is responsible
		//for further disassembling. the result of this is an object of type LexiconEntry
		//which can be added directly
		
		for(String rawEntry : rawEntries)
		{
			section.addLexiconEntry(LexiconEntry.fromString(rawEntry));
		}

		return section;
	}
}
