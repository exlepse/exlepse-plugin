package org.exlepse.xle.lexicon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.section.BaseSection;
import org.exlepse.xle.section.SectionHeader;

/**
 * <code>LexiconSection</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:50:43 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: LexiconSection.java 8209 2010-07-05 16:49:25Z raedle $
 * @since 1.0.0
 */
public class LexiconSection extends BaseSection {

	private Collection<LexiconEntry> lexiconEntries;

	public LexiconSection(SectionHeader header) {
		super(header);
		lexiconEntries = new ArrayList<LexiconEntry>();
	}
	
	/**
	 * @return the lexiconEntries
	 */
	public final Collection<LexiconEntry> getLexiconEntries() {
		return lexiconEntries;
	}

	public void addLexiconEntry(LexiconEntry lexiconEntry) {
		lexiconEntries.add(lexiconEntry);
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.section.BaseSection#printSectionContent(org.exlepse.xle.io.IDevice)
	 */
	@Override
	public void printSectionContent(IDevice device) throws IOException {
		for (LexiconEntry le : lexiconEntries) {
			le.print(device);
			device.write("\n\n");
		}
	}
}
