/**
 * 
 */
package org.exlepse.xle.lexicon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import org.exlepse.xle.AbstractItem;
import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.io.IPrintable;

/**
 * <code>LexiconEntry</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:50:33 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: LexiconEntry.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class LexiconEntry extends AbstractItem implements IPrintable {

	private String word;

	private ArrayList<LexiconSubEntry> subEntries;

	public LexiconEntry(String raw, String word) {
		super(raw);
		this.word = word;
		subEntries = new ArrayList<LexiconSubEntry>(1);
	}

	public void addSubEntry(LexiconSubEntry entry) {
		subEntries.add(entry);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 */
	public void print(IDevice device) throws IOException {
		try {
			device.write(word);

			int size = subEntries.size();
			int ctr = size;
			for (int i = 0; i < size; i++) {
				device.write('\t');
				subEntries.get(i).print(device);
				if (--ctr > 0)
					device.write(";\n");
			}

			device.write('.');
		} catch (Exception e) {
			throw new IOException("Could not write word '" + word
					+ "' to device.");
		}
	}

	private static boolean isWhitespace(char ch) {
		switch (ch) {
		case ' ':
			return true;
		case '\t':
			return true;
		case '\n':
			return true;
		case '\r':
			return true;
		default:
			return false;
		}
	}

	private static boolean isDoubleQuote(char ch) {
		if (ch == '"')
			return true;
		else
			return false;
	}

	public static LexiconEntry fromString(String rawEntry) {
		// first remove the dot at the end of the string
		rawEntry = rawEntry.substring(0, rawEntry.lastIndexOf('.'));

		// TODO: how will we handle comments ? (first approach: just delete
		// them)
		rawEntry = removeComment(rawEntry);

		String word = getWord(rawEntry);

		LexiconEntry entry = new LexiconEntry(rawEntry, word);

		rawEntry = rawEntry.substring(word.length());

		Scanner scanner = new Scanner(rawEntry);
		scanner.useDelimiter(";");

		while (scanner.hasNext()) {
			String part = scanner.next();
			entry.addSubEntry(LexiconSubEntry.fromString(part.trim()));
		}

		return entry;
	}

	private static String getWord(String rawEntry) {
		String word = "";

		for (char ch : rawEntry.toCharArray()) {
			if (isWhitespace(ch))
				break;
			else
				word += ch;
		}
		return word;
	}

	private static String removeComment(String rawEntry) {
		boolean commentOn = false;

		String withoutComment = "";

		for (char ch : rawEntry.toCharArray()) {
			if (isDoubleQuote(ch) && !commentOn) {
				commentOn = true;
				continue;
			}

			if (isDoubleQuote(ch) && commentOn) {

				commentOn = false;
				continue;
			}

			if (!commentOn) {
				withoutComment += ch;
			}
		}
		return withoutComment.trim();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return word;
	}
}
