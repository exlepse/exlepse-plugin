package org.exlepse.xle.lexicon;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.io.IPrintable;

public class LexiconSubEntry implements IPrintable {

	public void print(IDevice device) throws IOException {
		device.write(partOfSpeech);
		device.write(" ");
		device.write(fromMorphcode(morphcode));
		device.write(" ");
		device.write(schematas);
	}
	
	private String partOfSpeech;

	private Morphcodes morphcode;

	private String schematas;

	private static Morphcodes toMorphcode(String code)
	{
		if(code.equals("*"))
			return Morphcodes.STAR;
		else if (code.equals("XLE"))
			return Morphcodes.XLE;
		else
			return null;
	}
	
	private static String fromMorphcode(Morphcodes code)
	{
		switch (code) {
		case STAR:
			return "*";
		case XLE:
			return "XLE";
		default:
			return null;
		}
	}
	
	
	public static LexiconSubEntry fromString(String part) {

		Pattern pattern = Pattern.compile("([\\w]+)(\\s+)(\\*|XLE)((\\s+)(.*)|$)", Pattern.DOTALL);
		Matcher matcher = pattern.matcher(part);
		
		if(matcher.matches())
		{
			LexiconSubEntry subEntry = new LexiconSubEntry();
			subEntry.partOfSpeech = matcher.group(1);
			subEntry.morphcode = toMorphcode(matcher.group(3));
			subEntry.schematas = matcher.group(6);
			return subEntry;
		}
				
		return null;
		
	}

}
