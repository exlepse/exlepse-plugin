package org.exlepse.xle;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.io.IPrintable;
import org.exlepse.xle.section.ISection;

/**
 * <code>Document</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:56:27 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: XLEDocument.java 9160 2010-08-10 12:15:26Z raedle $
 * @since 1.0.0
 */
public class XLEDocument implements IPrintable {

	private Collection<ISection> sections;

	public XLEDocument() {
		sections = new ArrayList<ISection>();
	}

	public void addSection(ISection section) {
		sections.add(section);
	}
	
	public Collection<ISection> getSections() {
		return sections;
	}

	/**
	 * @param device
	 * @throws IOException
	 */
	public void print(IDevice device) throws IOException {

		for (ISection section : sections) {
			section.print(device);
			device.write("\n----\n");
		}
	}
}
