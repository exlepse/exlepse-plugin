/**
 * 
 */
package org.exlepse.xle;

/**
 * <code>IParser</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:56:38 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: IParser.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public interface IParser<T> {
	T parse(String str);
}
