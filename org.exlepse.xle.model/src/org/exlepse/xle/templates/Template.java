/**
 * 
 */
package org.exlepse.xle.templates;

import java.io.IOException;

import org.exlepse.xle.AbstractItem;
import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.io.IPrintable;

/**
 * @author raedle
 * 
 */
public class Template extends AbstractItem implements IPrintable {

	public static Template create(String str) {
		return new Template(str, str);
	}

	private final String content;

	public Template(String raw, String content) {
		super(raw);
		this.content = content;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 */
	public void print(IDevice device) throws IOException {
		device.write(content);
		device.write('.');
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		int idx = getMin(content, 30, '.');
		idx = getMin(content, idx, ' ');
		idx = getMin(content, idx, '(');
		idx = getMin(content, idx, '\t');
		idx = getMin(content, idx, '\r');
		idx = getMin(content, idx, '\n');
		return content.substring(0, idx);
	}

	private static int getMin(String content, int curMin, char c) {
		int idx = content.indexOf(c);
		if (idx != -1) {
			return Math.min(curMin, idx);
		}
		return curMin;
	}
}
