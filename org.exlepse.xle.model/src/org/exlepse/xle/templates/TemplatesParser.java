/**
 * 
 */
package org.exlepse.xle.templates;

import java.util.Scanner;

import org.exlepse.xle.IParser;
import org.exlepse.xle.section.SectionHeader;
import org.exlepse.xle.section.SectionParser;

/**
 * @author raedle
 * 
 */
public class TemplatesParser extends SectionParser implements IParser<TemplatesSection> {

	public TemplatesParser(SectionHeader sectionHeader) {
		super(sectionHeader);
	}

	/**
	 * @param content
	 * @return
	 */
	public TemplatesSection parse(String content) {

		TemplatesSection section = new TemplatesSection(header);

		Scanner scanner = new Scanner(content);
		scanner.useDelimiter("\\.");

		// Ignore TOY ENGLISH TEMPLATES (1.0)
		scanner.nextLine();

		while (scanner.hasNext()) {
			String template = scanner.next().trim();

			if (template.length() > 0) section.addTemplate(Template.create(template));
		}

		return section;
	}
}
