package org.exlepse.xle.templates;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.section.BaseSection;
import org.exlepse.xle.section.SectionHeader;

/**
 * <code>TemplatesSection</code>.
 *
 * <pre>
 * Date: Jul 2, 2010
 * Time: 4:14:43 PM
 * </pre>
 *
 * @author Roman Raedle
 * @author Michael Zoellner
 *
 * @version $Id: TemplatesSection.java 8209 2010-07-05 16:49:25Z raedle $
 * @since 1.0.0
 */
public class TemplatesSection extends BaseSection {

	private Collection<Template> templates;

	public TemplatesSection(SectionHeader header) {
		super(header);
		templates = new ArrayList<Template>();
	}
	
	/**
	 * @return the templates
	 */
	public final Collection<Template> getTemplates() {
		return templates;
	}

	public void addTemplate(Template template) {
		templates.add(template);
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.section.BaseSection#printSectionContent(org.exlepse.xle.io.IDevice)
	 */
	@Override
	public void printSectionContent(IDevice device) throws IOException {
		for (Template template : templates) {
			template.print(device);
			device.write("\n\n");
		}
	}
}
