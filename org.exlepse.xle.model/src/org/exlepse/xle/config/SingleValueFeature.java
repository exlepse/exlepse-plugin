package org.exlepse.xle.config;

import java.io.IOException;

import org.exlepse.xle.io.IDevice;

/**
 * <code>SingleValueFeature</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 6:23:57 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: SingleValueFeature.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public abstract class SingleValueFeature extends AbstractFeature {

	public SingleValueFeature(String raw, String name, String v) {
		super(raw, name);
		value = v;
	}

	private String value;

	public String getValue() {
		return value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.config.IFeature#getValues()
	 */
	public String[] getValues() {
		return new String[] { value };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.config.AbstractFeature#printValues(org.exlepse.xle.io.IDevice)
	 */
	public void printValues(IDevice device) throws IOException {
		device.write(value);
	}
}
