/**
 * 
 */
package org.exlepse.xle.config;

import java.io.IOException;

import org.exlepse.xle.AbstractItem;
import org.exlepse.xle.io.IDevice;

/**
 * <code>AbstractFeature</code>.
 *
 * <pre>
 * Date: Jun 29, 2010
 * Time: 10:02:15 PM
 * </pre>
 *
 * @author Roman Raedle
 * @author Michael Zoellner
 *
 * @version $Id: AbstractFeature.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public abstract class AbstractFeature extends AbstractItem implements IFeature {

	private String name;
	
	public AbstractFeature(String raw, String name) {
		super(raw);
		this.name = name;
	}
	
	/* (non-Javadoc)
	 * @see org.exlepse.xle.config.IFeature#getName()
	 */
	public final String getName() {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 */
	public final void print(IDevice device) throws IOException {
		device.write(name);
		device.write('\t');
		printValues(device);
	}
	
	/**
	 * @param device
	 * @throws IOException
	 */
	public abstract void printValues(IDevice device) throws IOException;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getName();
	}
}
