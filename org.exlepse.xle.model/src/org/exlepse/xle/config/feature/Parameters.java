package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SequenceFeature;

/**
 * <code>Parameters</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 7:10:58 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: Parameters.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class Parameters extends SequenceFeature implements IFeature {

	public static final String NAME = "PARAMETERS";

	public Parameters(String raw, String[] values) {
		super(raw, NAME, values);
	}

	public static IFeature parse(String rawFeature) {
		return new Parameters(rawFeature, ConfigParser.ExtractSequence(NAME, rawFeature));
	}
}
