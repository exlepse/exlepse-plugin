/**
 * 
 */
package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SingleValueFeature;

/**
 * <code>RootCat</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:55:23 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: RootCat.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class RootCat extends SingleValueFeature implements IFeature {

	public static final String NAME = "ROOTCAT";

	public RootCat(String raw, String value) {
		super(raw, NAME, value);
	}

	public static IFeature parse(String rawFeature) {
		return new RootCat(rawFeature, ConfigParser.ExtractSingleValue(NAME, rawFeature));
	}
}
