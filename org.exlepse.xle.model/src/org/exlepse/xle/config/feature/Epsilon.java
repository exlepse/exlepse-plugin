/**
 * 
 */
package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SingleValueFeature;

/**
 * <code>Epsilon</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:51:21 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: Epsilon.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class Epsilon extends SingleValueFeature implements IFeature {

	public static final String NAME = "EPSILON";

	public Epsilon(String raw, String value) {
		super(raw, NAME, value);
	}

	public static IFeature parse(String rawFeature) {
		return new Epsilon(rawFeature, ConfigParser.ExtractSingleValue(NAME, rawFeature));
	}
}
