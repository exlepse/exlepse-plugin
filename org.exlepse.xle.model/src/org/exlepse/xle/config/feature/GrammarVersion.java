package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SingleValueFeature;

/**
 * <code>GrammarVersion</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 7:12:27 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: GrammarVersion.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class GrammarVersion extends SingleValueFeature implements IFeature {

	public static final String NAME = "GRAMMARVERSION";

	public GrammarVersion(String raw, String value) {
		super(raw, NAME, value);
	}

	public static IFeature parse(String rawFeature) {
		return new GrammarVersion(rawFeature, ConfigParser.ExtractSingleValue(NAME, rawFeature));
	}
}
