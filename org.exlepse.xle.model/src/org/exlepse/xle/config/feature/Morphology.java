package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SectionFeature;

/**
 * <code>Morphology</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 7:11:48 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: Morphology.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class Morphology extends SectionFeature implements IFeature {

	public static final String NAME = "MORPHOLOGY";

	public Morphology(String raw, String[] values) {
		super(raw, NAME, values);
	}

	public static IFeature parse(String rawFeature) {
		return new Morphology(rawFeature, ConfigParser.ExtractSection(NAME, rawFeature));
	}
}
