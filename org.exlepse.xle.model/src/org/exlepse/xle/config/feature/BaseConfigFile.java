package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SingleValueFeature;

/**
 * <code>BaseConfigFile</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 7:04:19 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: BaseConfigFile.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class BaseConfigFile extends SingleValueFeature implements IFeature {

	public static final String NAME = "BASECONFIGFILE";

	public BaseConfigFile(String raw, String value) {
		super(raw, NAME, value);
	}

	public static IFeature parse(String s) {
		return new BaseConfigFile(s, ConfigParser.ExtractSingleValue(NAME, s));
	}
}
