package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SequenceFeature;

/**
 * <code>EncryptFiles</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 7:14:42 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: EncryptFiles.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class EncryptFiles extends SequenceFeature implements IFeature {

	public static final String NAME = "ENCRYPTFILES";

	public EncryptFiles(String raw, String[] value) {
		super(raw, NAME, value);
	}

	public static IFeature parse(String rawFeature) {
		return new EncryptFiles(rawFeature, ConfigParser.ExtractSequence(NAME, rawFeature));
	}
}
