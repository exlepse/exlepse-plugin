package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SectionFeature;

/**
 * <code>Features</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 7:13:12 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: Features.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class Features extends SectionFeature implements IFeature {

	public static final String NAME = "FEATURES";

	public Features(String raw, String[] values) {
		super(raw, NAME, values);
	}

	public static IFeature parse(String rawFeature) {
		return new Features(rawFeature, ConfigParser.ExtractSection(NAME, rawFeature));
	}
}
