package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SequenceFeature;

/**
 * <code>ExternalAttributes</code>.
 * 
 * <pre>
 * Date: Jul 5, 2010
 * Time: 7:13:30 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: ExternalAttributes.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class ExternalAttributes extends SequenceFeature implements IFeature {

	public static final String NAME = "EXTERNALATTRIBUTES";

	public ExternalAttributes(String raw, String[] value) {
		super(raw, NAME, value);
	}

	public static IFeature parse(String rawFeature) {
		return new ExternalAttributes(rawFeature, ConfigParser.ExtractSequence(NAME, rawFeature));
	}
}
