/**
 * 
 */
package org.exlepse.xle.config.feature;

import org.exlepse.xle.config.ConfigParser;
import org.exlepse.xle.config.IFeature;
import org.exlepse.xle.config.SectionFeature;

/**
 * <code>Rules</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:55:31 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: Rules.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class Rules extends SectionFeature implements IFeature {

	public static final String NAME = "RULES";

	public Rules(String raw, String[] values) {
		super(raw, NAME, values);
	}

	public static IFeature parse(String rawFeature) {
		return new Rules(rawFeature, ConfigParser.ExtractSection(NAME, rawFeature));
	}
}
