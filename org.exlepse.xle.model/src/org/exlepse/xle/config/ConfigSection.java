/**
 * 
 */
package org.exlepse.xle.config;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import org.exlepse.xle.config.feature.BaseConfigFile;
import org.exlepse.xle.config.feature.CharacterEncoding;
import org.exlepse.xle.config.feature.EncryptFiles;
import org.exlepse.xle.config.feature.Epsilon;
import org.exlepse.xle.config.feature.ExternalAttributes;
import org.exlepse.xle.config.feature.Features;
import org.exlepse.xle.config.feature.Files;
import org.exlepse.xle.config.feature.GenOptimalityOrder;
import org.exlepse.xle.config.feature.GovernableRelations;
import org.exlepse.xle.config.feature.GrammarVersion;
import org.exlepse.xle.config.feature.LexEntries;
import org.exlepse.xle.config.feature.MorphAccessPath;
import org.exlepse.xle.config.feature.Morphology;
import org.exlepse.xle.config.feature.NonDistributives;
import org.exlepse.xle.config.feature.OptimalityOrder;
import org.exlepse.xle.config.feature.OtherFiles;
import org.exlepse.xle.config.feature.Parameters;
import org.exlepse.xle.config.feature.PerformanceVarsFile;
import org.exlepse.xle.config.feature.ReparseCat;
import org.exlepse.xle.config.feature.RootCat;
import org.exlepse.xle.config.feature.Rules;
import org.exlepse.xle.config.feature.SemanticFunctions;
import org.exlepse.xle.config.feature.Templates;
import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.section.BaseSection;
import org.exlepse.xle.section.SectionHeader;

/**
 * <code>ConfigSection</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:56:12 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: ConfigSection.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class ConfigSection extends BaseSection {

	public static String[] ConfigFeatures = new String[] { BaseConfigFile.NAME,
			CharacterEncoding.NAME, EncryptFiles.NAME, Epsilon.NAME,
			ExternalAttributes.NAME, Features.NAME, Files.NAME,
			GenOptimalityOrder.NAME, GovernableRelations.NAME,
			GrammarVersion.NAME, LexEntries.NAME, MorphAccessPath.NAME,
			Morphology.NAME, NonDistributives.NAME, OptimalityOrder.NAME,
			OtherFiles.NAME, Parameters.NAME, PerformanceVarsFile.NAME,
			ReparseCat.NAME, RootCat.NAME, Rules.NAME, SemanticFunctions.NAME,
			Templates.NAME };

	private Map<String, IFeature> features;

	public ConfigSection(SectionHeader sh) {
		super(sh);
		features = new TreeMap<String, IFeature>(new Comparator<String>() {

			public int compare(String o1, String o2) {
				return o1.compareTo(o2);
			}
		});
	}

	public void addFeature(IFeature feature) throws Exception {

		String name = feature.getName();

		if (name.length() <= 0)
			throw new Exception("Feature has no name");

		if (!features.containsKey(name))
			features.put(name, feature);
		else
			throw new Exception("Feature " + name
					+ " already exists in Config Section");
	}

	public boolean hasFeature(String feature) {
		return features.containsKey(feature);
	}

	/**
	 * @return the features
	 */
	public final Collection<IFeature> getFeatures() {
		return features.values();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.exlepse.xle.section.BaseSection#printSectionContent(org.exlepse.xle
	 * .io.IDevice)
	 */
	@Override
	public void printSectionContent(IDevice device) throws IOException {
		for (IFeature feature : features.values()) {
			feature.print(device);
			device.write(".\n");
		}
	}
}
