/**
 * 
 */
package org.exlepse.xle.config;

import org.exlepse.xle.io.IPrintable;

/**
 * <code>IFeature</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:56:18 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: IFeature.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public interface IFeature extends IPrintable {
	String getName();
	String[] getValues();
}
