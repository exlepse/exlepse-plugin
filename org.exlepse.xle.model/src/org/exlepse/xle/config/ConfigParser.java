/**
 * 
 */
package org.exlepse.xle.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.util.StringTokenizer;

import org.exlepse.xle.IParser;
import org.exlepse.xle.config.feature.BaseConfigFile;
import org.exlepse.xle.config.feature.CharacterEncoding;
import org.exlepse.xle.config.feature.EncryptFiles;
import org.exlepse.xle.config.feature.Epsilon;
import org.exlepse.xle.config.feature.ExternalAttributes;
import org.exlepse.xle.config.feature.Features;
import org.exlepse.xle.config.feature.Files;
import org.exlepse.xle.config.feature.GenOptimalityOrder;
import org.exlepse.xle.config.feature.GovernableRelations;
import org.exlepse.xle.config.feature.GrammarVersion;
import org.exlepse.xle.config.feature.LexEntries;
import org.exlepse.xle.config.feature.MorphAccessPath;
import org.exlepse.xle.config.feature.Morphology;
import org.exlepse.xle.config.feature.NonDistributives;
import org.exlepse.xle.config.feature.OptimalityOrder;
import org.exlepse.xle.config.feature.OtherFiles;
import org.exlepse.xle.config.feature.Parameters;
import org.exlepse.xle.config.feature.PerformanceVarsFile;
import org.exlepse.xle.config.feature.ReparseCat;
import org.exlepse.xle.config.feature.RootCat;
import org.exlepse.xle.config.feature.Rules;
import org.exlepse.xle.config.feature.SemanticFunctions;
import org.exlepse.xle.config.feature.Templates;
import org.exlepse.xle.section.SectionHeader;
import org.exlepse.xle.section.SectionParser;

/**
 * <code>ConfigParser</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:55:53 PM
 * </pre>
 * 
 * @author Roman Raedle
 * @author Michael Zoellner
 * 
 * @version $Id: ConfigParser.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public class ConfigParser extends SectionParser implements
		IParser<ConfigSection> {

	public ConfigParser(SectionHeader sectionHeader) {
		super(sectionHeader);
	}

	/**
	 * TOY ENGLISH CONFIG (1.0) ROOTCAT S. FILES . LEXENTRIES (TOY ENGLISH).
	 * RULES (TOY ENGLISH). TEMPLATES (TOY ENGLISH). GOVERNABLERELATIONS SUBJ
	 * OBJ OBJ2 OBJ-TH OBL OBL-?+ COMP XCOMP. SEMANTICFUNCTIONS ADJUNCT TOPIC.
	 * NONDISTRIBUTIVES NUM PERS COORD-FORM COORD-TYPE STMT-TYPE. EPSILON e.
	 * 
	 * @param config
	 * @return
	 */
	public ConfigSection parse(String rawConfig) {
		Scanner scanner = new Scanner(rawConfig);
		scanner.useDelimiter("\\.\\s");

		// TOY ENGLISH CONFIG (1.0)
		scanner.nextLine();
		ConfigSection config = new ConfigSection(header);

		while (scanner.hasNext()) {
			try {
				String rawFeature = scanner.next().trim();
				if (rawFeature.length() <= 0)
					continue;
				IFeature feature = parseFeature(rawFeature);
				if (feature != null) {
					config.addFeature(feature);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		scanner.close();

		return config;
	}

	public static IFeature parseFeature(String rawFeature) {

		if (rawFeature.startsWith(BaseConfigFile.NAME))
			return BaseConfigFile.parse(rawFeature);

		if (rawFeature.startsWith(CharacterEncoding.NAME))
			return CharacterEncoding.parse(rawFeature);

		if (rawFeature.startsWith(EncryptFiles.NAME))
			return EncryptFiles.parse(rawFeature);

		if (rawFeature.startsWith(Epsilon.NAME))
			return Epsilon.parse(rawFeature);

		if (rawFeature.startsWith(ExternalAttributes.NAME))
			return ExternalAttributes.parse(rawFeature);

		if (rawFeature.startsWith(Features.NAME))
			return Features.parse(rawFeature);

		if (rawFeature.startsWith(Files.NAME))
			return Files.parse(rawFeature);

		if (rawFeature.startsWith(GenOptimalityOrder.NAME))
			return GenOptimalityOrder.parse(rawFeature);

		if (rawFeature.startsWith(GovernableRelations.NAME))
			return GovernableRelations.parse(rawFeature);

		if (rawFeature.startsWith(GrammarVersion.NAME))
			return GrammarVersion.parse(rawFeature);

		if (rawFeature.startsWith(LexEntries.NAME))
			return LexEntries.parse(rawFeature);

		if (rawFeature.startsWith(MorphAccessPath.NAME))
			return MorphAccessPath.parse(rawFeature);

		if (rawFeature.startsWith(Morphology.NAME))
			return Morphology.parse(rawFeature);

		if (rawFeature.startsWith(NonDistributives.NAME))
			return NonDistributives.parse(rawFeature);

		if (rawFeature.startsWith(OptimalityOrder.NAME))
			return OptimalityOrder.parse(rawFeature);

		if (rawFeature.startsWith(OtherFiles.NAME))
			return OtherFiles.parse(rawFeature);

		if (rawFeature.startsWith(Parameters.NAME))
			return Parameters.parse(rawFeature);

		if (rawFeature.startsWith(PerformanceVarsFile.NAME))
			return PerformanceVarsFile.parse(rawFeature);

		if (rawFeature.startsWith(ReparseCat.NAME))
			return ReparseCat.parse(rawFeature);

		if (rawFeature.startsWith(RootCat.NAME))
			return RootCat.parse(rawFeature);

		if (rawFeature.startsWith(Rules.NAME))
			return Rules.parse(rawFeature);

		if (rawFeature.startsWith(SemanticFunctions.NAME))
			return SemanticFunctions.parse(rawFeature);

		if (rawFeature.startsWith(Templates.NAME))
			return Templates.parse(rawFeature);

		return null;

	}

	public static String ExtractSingleValue(String featureName, String rawValue) {
		return rawValue.substring(featureName.length(), rawValue.length())
				.trim();
	}

	public static String[] ExtractSequence(String featureName, String rawValue) {
		StringTokenizer tokenizer = new StringTokenizer(ExtractSingleValue(
				featureName, rawValue));

		return ExtractSequenceWithTokenizer(tokenizer);
	}

	public static String[] ExtractSection(String featureName, String rawValue) {
		StringTokenizer tokenizer = new StringTokenizer(ExtractSingleValue(
				featureName, rawValue), "()");

		return ExtractSequenceWithTokenizer(tokenizer);
	}

	private static String[] ExtractSequenceWithTokenizer(
			StringTokenizer tokenizer) {
		Collection<String> values = new ArrayList<String>();

		while (tokenizer.hasMoreTokens()) {
			String token = tokenizer.nextToken().trim();
			if (token.length() > 0) {
				values.add(token);
			}
		}

		return values.toArray(new String[0]);
	}

}
