package org.exlepse.xle.config;

import java.io.IOException;

import org.exlepse.xle.io.IDevice;

/**
 * <code>SequenceFeature</code>.
 *
 * <pre>
 * Date: Jul 5, 2010
 * Time: 6:23:41 PM
 * </pre>
 *
 * @author Michael Zoellner
 * @author Roman Raedle
 *
 * @version $Id: SequenceFeature.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public abstract class SequenceFeature extends AbstractFeature {

	private String[] values;

	public SequenceFeature(String raw, String name, String[] v) {
		super(raw, name);
		values = v;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.config.AbstractFeature#printValues(org.exlepse.xle.io.IDevice)
	 */
	public void printValues(IDevice device) throws IOException {
		for (int i = 0; i < values.length; i++) {
			device.write(values[i]);
			device.writeWhitespace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.config.IFeature#getValues()
	 */
	public String[] getValues() {
		return values;
	}
}
