package org.exlepse.xle.config;

import java.io.IOException;

import org.exlepse.xle.io.IDevice;

/**
 * <code>SectionFeature</code>.
 *
 * <pre>
 * Date: Jul 5, 2010
 * Time: 6:23:31 PM
 * </pre>
 *
 * @author Michael Zoellner
 * @author Roman Raedle
 *
 * @version $Id: SectionFeature.java 9226 2010-08-12 17:25:45Z raedle $
 * @since 1.0.0
 */
public abstract class SectionFeature extends SequenceFeature {

	public SectionFeature(String raw, String name, String[] values) {
		super(raw, name, values);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.exlepse.xle.config.SequenceFeature#printValues(org.exlepse.xle.io.IDevice)
	 */
	@Override
	public void printValues(IDevice device) throws IOException {

		String[] values = getValues();

		for (int i = 0; i < values.length; i++) {
			device.write("(");
			device.write(values[i]);
			device.writeWhitespace();
			device.write(")");
		}
	}
}
