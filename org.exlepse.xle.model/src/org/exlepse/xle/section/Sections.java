package org.exlepse.xle.section;

/**
 * <code>Sections</code>.
 *
 * Date: Jun 29, 2010
 * Time: 9:39:56 PM
 *
 * @author Michael Zoellner
 * @author Roman Raedle
 *
 * @version 1.0.0
 * $Id: Sections.java 8095 2010-07-02 11:31:19Z raedle $
 */
public enum Sections {
	CONFIG,
	RULES,
	TEMPLATES,
	LEXICON,
	FEATURES,
	MORPHOLOGY
}
