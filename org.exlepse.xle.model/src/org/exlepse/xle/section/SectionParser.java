package org.exlepse.xle.section;

/**
 * <code>SectionParser</code>.
 *
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:40:38 PM
 * </pre>
 *
 * @author Roman Raedle
 * @author Michael Zoellner
 *
 * @version $Id: SectionParser.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public class SectionParser {

	protected SectionHeader header;

	public SectionParser(SectionHeader sectionHeader) {
		header = sectionHeader;
	}
}
