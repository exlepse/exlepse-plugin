package org.exlepse.xle.section;

public class RawSectionParser extends SectionParser {

	public RawSectionParser(SectionHeader sectionHeader) {
		super(sectionHeader);
	}
	
	public RawSection parse(String content)
	{
		RawSection section = new RawSection(header);
		
		section.RawContent = content;
		
		return section;
	}

}
