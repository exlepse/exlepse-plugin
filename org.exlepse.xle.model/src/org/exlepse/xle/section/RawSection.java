package org.exlepse.xle.section;

import java.io.IOException;

import org.exlepse.xle.io.IDevice;

/**
 * <code>RawSection</code>.
 *
 * <pre>
 * Date: Jul 5, 2010
 * Time: 6:30:09 PM
 * </pre>
 *
 * @author Michael Zoellner
 * @author Roman Raedle
 *
 * @version $Id: RawSection.java 8209 2010-07-05 16:49:25Z raedle $
 * @since 1.0.0
 */
public class RawSection extends BaseSection {

	public String RawContent;
	
	public RawSection(SectionHeader sectionHeader) {
		super(sectionHeader);
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.section.BaseSection#printSectionContent(org.exlepse.xle.io.IDevice)
	 */
	@Override
	public void printSectionContent(IDevice device) throws IOException {
		device.write(RawContent);
	}
}
