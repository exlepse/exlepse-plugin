package org.exlepse.xle.section;

import java.io.IOException;

import org.exlepse.xle.io.IDevice;

/**
 * <code>BaseSection</code>.
 * 
 * Date: Jun 29, 2010 Time: 9:38:10 PM
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version 1.0.0 $Id: BaseSection.java 9160 2010-08-10 12:15:26Z raedle $
 */
public abstract class BaseSection implements ISection {

	private SectionHeader header;

	public BaseSection(SectionHeader sectionHeader) {
		header = sectionHeader;
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.section.ISection#getSectionType()
	 */
	public Sections getSectionType() {
		return header.getSectionType();
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.section.ISection#getHeader()
	 */
	public SectionHeader getHeader() {
		return header;
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 */
	public final void print(IDevice device) throws IOException {
		getHeader().print(device);
		device.write("\n\n");
		
		// Prints the content of the section.
		printSectionContent(device);
	}
	
	/**
	 * @param device
	 * @throws IOException
	 */
	protected abstract void printSectionContent(IDevice device) throws IOException;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getSectionType().toString();
	}
}
