package org.exlepse.xle.section;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.exlepse.xle.IParser;

/**
 * <code>SectionHeaderParser</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:42:05 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: SectionHeaderParser.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public class SectionHeaderParser implements IParser<SectionHeader> {

	/* (non-Javadoc)
	 * @see org.exlepse.xle.IParser#parse(java.lang.String)
	 */
	public SectionHeader parse(String header) {

		Pattern pattern = Pattern.compile("(\\w+)\\s+(\\w+)\\s+(\\w+)\\s+(\\(1\\.0\\))");

		Matcher matcher = pattern.matcher(header);

		SectionHeader sectionHeader = new SectionHeader();

		if (matcher.find()) {
			sectionHeader.setName1(matcher.group(1));
			sectionHeader.setName2(matcher.group(2));
			sectionHeader.setSectionType(matcher.group(3));
			sectionHeader.setXLEVersion(matcher.group(4));
		}

		return sectionHeader;
	}

}
