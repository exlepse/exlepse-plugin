package org.exlepse.xle.section;

import java.io.IOException;

import org.exlepse.xle.io.IDevice;
import org.exlepse.xle.io.IPrintable;

/**
 * <code>SectionHeader</code>.
 * 
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:43:57 PM
 * </pre>
 * 
 * @author Michael Zoellner
 * @author Roman Raedle
 * 
 * @version $Id: SectionHeader.java 9160 2010-08-10 12:15:26Z raedle $
 * @since 1.0.0
 */
public class SectionHeader implements IPrintable {

	private String name1;
	private String name2;
	private String xleVersion;
	private Sections sectionType;

	public String getName1() {
		return name1;
	}

	public String getName2() {
		return name2;
	}

	public String getXLEVersion() {
		return xleVersion;
	}

	public Sections getSectionType() {
		return sectionType;
	}

	public void setName1(String content) {
		name1 = content;
	}

	public void setName2(String content) {
		name2 = content;
	}

	public void setSectionType(String content) {
		sectionType = sectionStringToSectionType(content);
	}

	public void setXLEVersion(String content) {
		xleVersion = content;
	}

	private Sections sectionStringToSectionType(String content) {
		Sections sections = Sections.valueOf(content);
		
		if (sections == null) {
			throw new IllegalArgumentException("Section type " + content + " does not exist.");
		}
		return sections;
	}

	private String sectionTypeToSectionString(Sections section) {
		return section.toString();
	}

	/* (non-Javadoc)
	 * @see org.exlepse.xle.io.IPrintable#print(org.exlepse.xle.io.IDevice)
	 */
	public void print(IDevice device) throws IOException {
		device.write(name1);
		device.writeWhitespace();
		device.write(name2);
		device.writeWhitespace();
		device.write(sectionTypeToSectionString(sectionType));
		device.writeWhitespace();
		device.write(xleVersion);
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(name1);
		sb.append(" ");
		sb.append(name2);
		sb.append(" ");
		sb.append(sectionTypeToSectionString(sectionType));
		sb.append(" ");
		sb.append(xleVersion);

		return sb.toString();
	}
}
