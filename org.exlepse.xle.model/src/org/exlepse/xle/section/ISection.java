/**
 * 
 */
package org.exlepse.xle.section;

import org.exlepse.xle.io.IPrintable;

/**
 * <code>ISection</code>.
 *
 * <pre>
 * Date: Jun 29, 2010
 * Time: 9:49:01 PM
 * </pre>
 *
 * @author Roman Raedle
 * @author Michael Zoellner
 *
 * @version $Id: ISection.java 8095 2010-07-02 11:31:19Z raedle $
 * @since 1.0.0
 */
public interface ISection extends IPrintable {
	Sections getSectionType();
	SectionHeader getHeader();
}
