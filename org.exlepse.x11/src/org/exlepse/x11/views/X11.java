/**
 * 
 */
package org.exlepse.x11.views;

import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.ConnectException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.awt.SWT_AWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.part.ViewPart;

import com.jcraft.weirdx.WeirdX;

/**
 * @author raedle
 *
 */
public class X11 extends ViewPart {

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(final Composite parent) {
		// TODO Auto-generated method stub
//		new Thread() {
//			public void run() {
				try {
					final WeirdX weirdX = WeirdX.doWeirdStuff();

					Composite composite = new Composite(parent, SWT.NO_BACKGROUND | SWT.EMBEDDED);
					final Frame container2 = SWT_AWT.new_Frame(composite);
//					final Frame container = new Frame("WeirdX Container");
//
//					((Frame) container).addWindowListener(new WindowAdapter() {
//						public void windowClosed(WindowEvent e) {
//							System.exit(0);
//						}
//
//						public void windowClosing(WindowEvent e) {
//							((Frame) e.getWindow()).dispose();
//							System.exit(0);
//						}
//					});
					
					weirdX.weirdx_start(container2);
				} catch (ConnectException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
//			};
//		}.start();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		// ignore
	}
}
