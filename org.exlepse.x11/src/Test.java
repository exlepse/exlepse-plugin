import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;


public class Test {

	public static void main(String[] args) throws Exception {
		ProcessBuilder pb = new ProcessBuilder("/Users/raedle/Downloads/xfst-2.10.43/xfst");
//		ProcessBuilder pb = new ProcessBuilder("sh");
		
		Process process = pb.start();
//		Process process = Runtime.getRuntime().exec("/Users/raedle/Downloads/xfst-2.10.43/xfst");
		openErrorStreamThread(process.getErrorStream());
		openInputStreamThread(process.getInputStream());
		
		process.getOutputStream().write("source /Users/raedle/Downloads/sample-morph/latin-script.txt\n".getBytes());
		process.getOutputStream().write("exit\n".getBytes());
		process.getOutputStream().flush();
		
		process.waitFor();
		process.destroy();
	}
	
	/**
	 * @param errorStream
	 */
	protected static void openErrorStreamThread(final InputStream errorStream) {
		Thread errorStreamThread = new Thread() {

			@Override
			public void run() {
				Reader reader = new InputStreamReader(errorStream);
				int c;
				try {
					while ((c = reader.read()) != -1) {
						System.err.print((char) c);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		};

		errorStreamThread.start();
	}
	
	/**
	 * @param inputStream
	 */
	protected static void openInputStreamThread(final InputStream inputStream) {
		new Thread() {

			@Override
			public void run() {
				Reader reader = new InputStreamReader(inputStream);
				int c;
				try {
					while ((c = reader.read()) != -1) {
						System.out.print((char) c);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}.start();
	}
}
